<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Gestion des responsables</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() ?>">Accueil</a>
            </li>
            <li>
                <a href="<?php echo site_url() ?>/welcome/listeresponsable">Responsables</a>
            </li>
            <li class="active">
                <strong>Ajout responsables</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3>Formulaire d'ajout de responsables <small></small></h3>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <?php //var_dump($agents) ?>
                <div class="ibox-content">
                    <form method="post" action="http://localhost/apimanagecustomer/public/addRespo" class="form-horizontal" id="form-ajout-responsable">
                        <div class="form-group">
                            <h5 class="col-sm-3">INFOS DU RESPONSABLE</h5>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">Titre Responsabilité</label>
                            <div class="col-sm-8"><input type="text" name="titreresponsabilite" class="form-control" ></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">RESPONSABLE</label>
                            <div class="col-sm-8">
                                <select data-placeholder="Selectionnez le responsbale de cet agent..." class="chosen-select" required="" name="nomresponsable">
                                    <option></option>
                                    <?php foreach ($agents as $agent): ?>
                                        <option value="<?php echo $agent['idPersonne'] ?>"><?php echo $agent['NomPersonne'] . " " . $agent['PrenomPersonne'] ?></option>
                                    <?php endforeach ?>
                                </select>

                            </div>
                        </div>
                       
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <h5 class="col-sm-3">INFORMATION SUR LES AGENTS</h5>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">LISTE AGENTS</label>
                            <div class="col-sm-8">
                                <select data-placeholder="Selectionnez le responsbale de cet agent..." multiple="" required="" class="chosen-select" id="listagent" name="listagent[]">
                                    <?php foreach ($agents as $agent): ?>
                                    <option value="<?php echo $agent['idPersonne']?>"><?php echo $agent['NomPersonne'] . " " . $agent['PrenomPersonne'] ?></option>
                                    <?php endforeach ?>
                                </select>
                                <span class="help-block m-b-none"></span>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <?php //foreach ($agents as $agent): ?>
                                <label class="col-sm-3 control-label"><?php //echo $agent['NomPersonne'] . " " . $agent['PrenomPersonne'] ?></label>
                                <div class="col-sm-8">
                                    <input type="checkbox" name="agents[]" value="<?php //echo $agent['idPersonne'] ?>" class="form-control" >
                                </div>
                            <?php //endforeach ?>
                        </div> -->
                        <div></div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-3">
                                <button class="btn btn-white" type="reset">Vider les champs</button>
                                <button class="btn btn-primary" id="" onclick="alert($('#listagent').val())">Enregistrer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>