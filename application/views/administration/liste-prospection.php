<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Gestion des prospections</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() ?>">Accueil</a>
            </li>
            <li class="active">
                <strong>Prospections</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Opération sur les prospections</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-success btn-rounded" href="<?php echo site_url(); ?>/welcome/ajoutprospection">
                                <i class="fa fa-plus"> </i>&nbsp;&nbsp;Ajouter une prospection
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-primary btn-rounded">
                                <i class="fa fa-edit"> </i>&nbsp;&nbsp; Modifier la prospection
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-primary btn-rounded">
                                <i class="fa fa-eye"> </i>&nbsp;&nbsp; Voir détails de prospection
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-danger btn-rounded">
                                <i class="fa fa-trash"> </i>&nbsp;&nbsp; Supprimer la prospection
                            </a>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>LISTE DES RÔLES</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php //var_dump($prospections)?>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="table-liste-role" >
                            <thead>
                                <tr>
                                    <th>Date de prospection</th>
                                    <th>Noms du prospect</th>
                                    <th>Entreprise</th>
                                    <th>Contacts</th>
                                    <th>produit proposé</th>
                                    <th>Agent</th>
                                    <th>Date Rendez Vous</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($prospections as $prospection): ?>
                                    <tr class="gradeX">

                                        <td class="center"><?php echo date("d-m-Y", strtotime($prospection['dateprospection'])) ?></td>
                                        <td class="center"><?php echo $prospection['nomprospection'] . ' ' . $prospection['prenomprospection'] ?></td>
                                        <td class="center"><?php echo $prospection['societeprospection'] ?></td>
                                        <td class="center"><?php echo $prospection['telephoneprospection'] . '<br>' . $prospection['emailprospection'] ?></td>
                                        <td class="center"><?php echo $prospection['Libelleproduits'] ?></td>
                                        <td class="center"><?php echo $prospection['NomPersonne'] . ' ' . $prospection['PrenomPersonne'] ?></td>
                                        <td class="center"><?php echo date("d-m-Y", strtotime($prospection['dateRDVprospection'])) ?></td>
                                        <td class="center">
                                            <?php if ($prospection['idPersonne'] == $_SESSION['idUser']): ?>
                                                <a href="modifierprospection?d=<?php echo base64_encode($prospection['idprospection']) ?>" class="btn btn-danger" title="modifier la prospection" id="" ><span class="fa fa-edit"></span></a>
                                                <a href="#" class="btn btn-success" title="supprimer la prospection" onclick="deletepros(<?php echo $prospection['idprospection'] ?>);" id="<?php echo $prospection['dateprospection'] ?>" ><span class="fa fa-archive"></span></a>
                                            <?php endif; ?>
                                        </td>
                                    </tr> 
                                <?php endforeach ?>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>