<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Gestion des responsables</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() ?>">Accueil</a>
            </li>
            <li class="active">
                <strong>Responsables</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Opération sur les responsables</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-success btn-rounded" href="<?php echo site_url(); ?>/welcome/ajoutresponsable">
                                <i class="fa fa-plus"> </i>&nbsp;&nbsp;Ajouter un responsable
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-primary btn-rounded">
                                <i class="fa fa-edit"> </i>&nbsp;&nbsp; Modifier le responsable
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-primary btn-rounded">
                                <i class="fa fa-eye"> </i>&nbsp;&nbsp; Voir détails du responsable
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-danger btn-rounded">
                                <i class="fa fa-trash"> </i>&nbsp;&nbsp; Supprimer le responsable
                            </a>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>LISTE DES RESPONSABLES</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php /*var_dump($agents);$agents = $agents[0]*/?>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="table-liste-role" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nom et Prenom </th>
                                    <th>Titre responsabilité</th>
                                    <th>Nombre agent</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($agents as $agent): ?>
                                    <tr class="gradeX">
                                        <td><?php echo $agent['idPersonne']?></td>
                                        <td>
                                            <?php echo $agent['NomPersonne']." ".$agent['PrenomPersonne']?>
                                        </td>
                                        <td><?php echo $agent['titreresponsabilite']?></td>
                                        <td class="center">4</td>
                                        <td class="center">
                                            <a href="modifierResponsable" class="btn btn-success"><span class="fa fa-archive"></span></a>
                                            <a href="#" class="btn btn-danger"><span class="fa fa-wrench"></span></a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>

                            </tbody>

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>