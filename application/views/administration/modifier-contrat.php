<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Gestion des contrats</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() ?>">Accueil</a>
            </li>
            <li>
                <a href="<?php echo site_url() ?>/welcome/listeagent">Clients</a>
            </li>
            <li class="active">
                <strong>Ajout agent</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3>Formulaire d'ajout de contrats <small></small></h3>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" id="form-add-contrat" action="http://localhost/apimanagecustomer/public/updateContrat">
                        <div class="form-group">
                            <h5 class="col-sm-3">INFOS DE L'AGENT</h5>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">AGENT</label>
                            <div class="col-sm-8">

                                <select data-placeholder="Selectionnez le client..." class="chosen-select" name="Agent">
                                    <option value="<?php echo $idAagent ?>"><?php echo $nomAgent ?></option>
                                </select>
                                <span class="help-block m-b-none">Sélectionner l'agent iniateur du contrat</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <h5 class="col-sm-3">INFOS DU CLIENT</h5>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">CLIENT</label>
                            <div class="col-sm-8">
                                <?php //var_dump($contrat)?>
                                <select data-placeholder="Selectionnez le client..." class="chosen-select" name="Client">
                                    <?php foreach ($clients as $client): ?>
                                        <option value="<?php echo $client['idClient'] ?>" <?php if($client['idClient'] == $contrat['idclient']) echo 'selected' ?> ><?php echo $client['NomClient'] . " " . $client['PrenomClient'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block m-b-none">Sélectionner le client souscripteur du contrat</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <h5 class="col-sm-3">CHOIX DE PRODUITS</h5>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">PRODUITS</label>
                            <div class="col-sm-8">
                                <select data-placeholder="Selectionnez le produit..." class="chosen-select" name="Produit">
                                    <?php foreach ($produits as $produit): ?>
                                        <option value="<?php echo $produit['idproduits'] ?>" <?php if($produit['idproduits'] == $contrat['idproduit']) echo 'selected' ?> ><?php echo $produit['Libelleproduits'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block m-b-none">Sélectionner le produit souscrits au contrat</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <h5 class="col-sm-3">INFOS DU CONTRAT</h5>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">DATE ETABLISSEMENT</label>
                            <div class="col-sm-6"><input type="date" class="form-control" value="<?php echo $contrat['dateContrat'] ?>" name="dateContrat"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">DELAI DU CONTRAT</label>
                            <div class="col-sm-3"><label>date debut</label><input type="date" class="form-control" value="<?php echo $contrat['datedebutContrat'] ?>" name="datedebut"></div>
                            <div class="col-sm-3"><label>date fin</label><input type="date" class="form-control" value="<?php echo $contrat['datefinContrat'] ?>" name="datefin"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">MODE DE PAIMENT</label>
                            <div class="col-sm-9">
                                <?php foreach($modePaies as $modePaie):?>
                                <div class="i-checks"><label><input type="radio" class="form-control" value="<?php echo $modePaie['idmodePaiement']?>" <?php if($modePaie['idmodePaiement'] == $contrat['modepaiement']) echo 'checked' ?> name="modepaiement"><i></i><?php echo $modePaie['LibellemodePaiement']?></label></div>
                                <?php endforeach;?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">PERIODICITE DE PAIEMENT</label>
                            <div class="col-sm-6">
                                <select data-placeholder="Selectionnez la periode de versement..." class="chosen-select" name="periode">
                                    <?php foreach ($periodes as $periode): ?>
                                        <option value="<?php echo $periode['idPeriodicte'] ?>" <?php if($periode['idPeriodicte'] == $contrat['periodicite']) echo 'selected' ?>><?php echo $periode['libellePeriodicite'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">QUANTITÉ DE PRODUIT</label>
                            <div class="col-sm-6"><input type="text" class="form-control" name="quantiteproduit" value="<?php echo $contrat['quantiteproduit'] ?>"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">MONTANT TOTAL CONTRAT</label>
                            <div class="col-sm-6"><input type="text" class="form-control money" name="montanttotal" value="<?php echo $contrat['montantContrat'] ?>"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">MONTANT COMMISSION</label>
                            <div class="col-sm-6"><input type="text" value="" class="form-control" name="montantprime" readonly=""></div>
                            <div class="col-sm-6"><input type="hidden" value="<?php echo $contrat['idcontrat'] ?>" class="form-control" name="idContrat" readonly=""></div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-3">
                                <button class="btn btn-white" type="reset">Vider les champs</button>
                                <button class="btn btn-primary" type="submit">Enregistrer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>