<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Gestion des clients</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() ?>">Accueil</a>
            </li>
            <li>
                <a href="<?php echo site_url() ?>/welcome/listeagent">Clients</a>
            </li>
            <li class="active">
                <strong>Ajout client</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3>Formulaire d'ajout de clients <small></small></h3>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">


                    <div class="form-group">
                        <label class="col-sm-3 control-label">Que voulez-vous faire ?</label>
                        <div class="col-sm-8">
                            <select data-placeholder="Selectionnez le rôle de cet client..." class="chosen-select" id="choixAction">
                                <option value="1">CRÉER UN NOUVEAU CLIENT</option>
                                <option value="2">SÉLECTIONNER UN PROSPECT</option>
                            </select>

                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <form method="post" action="http://localhost/apimanagecustomer/public/addClient" class="form-horizontal" id="formNewClient" enctype="multipart/form-data">
                        <div id="">

                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-3 control-label">NOM</label>
                                <div class="col-sm-8"><input type="text" class="form-control" name="nomclient"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-3 control-label">PRÉNOMS</label>
                                <div class="col-sm-8"><input type="text" class="form-control" name="prenomclient"></div>
                                <div class="col-sm-8"><input type="hidden" value="<?php echo $_SESSION['idUser'] ?>" class="form-control" name="idagent"></div>
                                <div class="col-sm-8"><input type="hidden" value="<?php echo $_SESSION['idsociete'] ?>" class="form-control" name="idsociete"></div>
                                <div class="col-sm-8"><input type="hidden" value="<?php echo 0 ?>" class="form-control" name="idprospection"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-3 control-label">DATE & LIEU DE NAISSANCE </label>
                                <div class="col-sm-4">
                                    DATE
                                    <input type="date" placeholder="Né le..." class="form-control" name="datenaissClient">
                                </div>
                                <div class="col-sm-4">
                                    LIEU
                                    <input type="text" placeholder="à..." class="form-control" name="lieuNaisssClient">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-3 control-label">SOCIÉTÉ</label>
                                <div class="col-sm-8"><input type="text" class="form-control" name="societeclient"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-3 control-label">ADRESSE</label>
                                <div class="col-sm-8"><input type="text" class="form-control" name="adresseclient"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-3 control-label">FONCTION</label>
                                <div class="col-sm-8"><input type="text" class="form-control" name="fonctionclient"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-3 control-label">TÉLÉPHONE</label>
                                <div class="col-sm-8"><input type="text" class="form-control" name="telephoneclient"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-3 control-label">EMAIL</label>
                                <div class="col-sm-8"><input type="email" class="form-control" name="emailclient"></div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <label class="col-sm-3 control-label">PIECE JUSTIFICATIVES</label>
                            <div class="col-sm-8">
                                <table class="table table-striped table-bordered table-hover" id="">
                                    <thead>
                                        <tr>
                                            <td>#</td>
                                            <td>libelle de la pièce</td>
                                            <td>Numero de la pièce</td>
                                            <td>Fichier</td>
                                            <td>Action</td>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody1">
                                        <tr>
                                            <td></td>
                                            <td>
                                                <select data-placeholder="Selectionnez le rôle de cet client..." class="chosen-select" name="typepiece">
                                                    <option value="CNI">CNI</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" placeholder="entrez le numero de la pièce" class="form-control" name="numeroPiece">
                                            </td>
                                            <td>
                                                <input type="file" placeholder="télécharger le fichier" class="form-control" name="imagePiece[]" id="imagePiece">
                                            </td>
                                            <td>
                                                <button class="btn btn-circle btn-warning" title="supprimer le fichier"><span class="fa fa-eraser"></span></button>
                                                <button class="btn btn-circle btn-primary"><span class="fa fa-plus"></span></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <label class="col-sm-8 control-label"></label>
                            <div class="col-sm-3">
                                <button class="btn btn-primary" onclick="addDocumentTab1()"><span class="fa fa-plus"></span> Ajouter document</button>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <br>
                            <br>
                            <br>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-3">
                                    <button class="btn btn-white" type="reset">Vider les champs</button>
                                    <button class="btn btn-primary">Enregistrer</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="hr-line-dashed"></div>
                    
                    <form method="post" action="http://localhost/apimanagecustomer/public/addClient" class="form-horizontal" id="formSelectProspect">
                        
                        <?php if(count($prospections)>= 1):?>
                        <div id="">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">NOM & PRENOM DU PROSPECT</label>
                                <div class="col-sm-8">
                                    <select data-placeholder="Selectionnez le rôle de cet client..." class="chosen-select" id='idprospect' name="idprospection" onchange='choiceprospect()'>
                                        <?php foreach ($prospections as $prospection): ?>
                                            <option value="<?php echo $prospection['idprospection'] ?>"><?php echo $prospection['nomprospection'] . ' ' . $prospection['prenomprospection'] ?></option>
                                        <?php endforeach; ?>
                                    </select>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8"><input type="hidden" class="form-control" name="nomclient" value="<?php echo $prospection['nomprospection'] ?>" id="idnomprospect"></div>
                                <div class="col-sm-8"><input type="hidden" class="form-control" name="prenomclient" value="<?php echo $prospection['prenomprospection'] ?>" id="idprenomprospect"></div>
                                <div class="col-sm-8"><input type="hidden" value="<?php echo $_SESSION['idUser'] ?>" class="form-control" name="idagent"></div>
                                <div class="col-sm-8"><input type="hidden" value="<?php echo $_SESSION['idsociete'] ?>" class="form-control" name="idsociete"></div>

                            </div>
                            <div class="form-group"><label class="col-sm-3 control-label">DATE & LIEU DE NAISSANCE </label>
                                <div class="col-sm-4">
                                    DATE
                                    <input type="date" placeholder="Né le..." class="form-control" name="datenaissClient">
                                </div>
                                <div class="col-sm-4">
                                    LIEU
                                    <input type="text" placeholder="à..." class="form-control" name="lieuNaisssClient">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-3 control-label">ADRESSE</label>
                                <div class="col-sm-8"><input type="text" class="form-control" name="adresseclient"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-3 control-label">SOCIÉTÉ</label>
                                <div class="col-sm-8"><input type="text" class="form-control" readonly="" id="societeprospect" name="societeclient"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-3 control-label">FONCTION</label>
                                <div class="col-sm-8"><input type="text" class="form-control" readonly="" id="fonctionprospect" name="fonctionclient"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-3 control-label">TÉLÉPHONE</label>
                                <div class="col-sm-8"><input type="text" class="form-control" id="telephoneprospect" name="telephoneclient"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-3 control-label">EMAIL</label>
                                <div class="col-sm-8"><input type="email" class="form-control" id="emailprospect" name="emailclient"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <label class="col-sm-3 control-label">PIECE JUSTIFICATIVES</label>
                            <div class="col-sm-8">
                                <table class="table table-striped table-bordered table-hover" id="">
                                    <thead>
                                        <tr>
                                            <td>#</td>
                                            <td>libelle de la pièce</td>
                                            <td>Numero de la pièce</td>
                                            <td>Fichier</td>
                                            <td>Action</td>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody2">
                                        <tr>
                                            <td></td>
                                            <td>
                                                <select data-placeholder="Selectionnez le rôle de cet client..." class="chosen-select">
                                                    <option value="">CNI</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" placeholder="entrez le numero de la pièce" class="form-control">
                                            </td>
                                            <td>
                                                <input type="file" placeholder="télécharger le fichier" class="form-control">
                                            </td>
                                            <td>
                                                <button class="btn btn-circle btn-warning" title="supprimer le fichier"><span class="fa fa-eraser"></span></button>
                                                <button class="btn btn-circle btn-primary"><span class="fa fa-plus"></span></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <label class="col-sm-8 control-label"></label>
                            <div class="col-sm-3">
                                <button class="btn btn-primary" onclick="addDocumentTab2()"><span class="fa fa-plus"></span> Ajouter document</button>
                            </div>
                        </div>
                        
                        
                        <div class="hr-line-dashed"></div>
                        <br>
                        <br>
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-3">
                                <button class="btn btn-white" type="reset">Vider les champs</button>
                                <button class="btn btn-primary" >Enregistrer</button>
                            </div>
                        </div>
                        <?php endif;?>
                        <?php if(count($prospections)== 0):?>
                                <h1 class="text text-center text-warning"> Désolé, vous n'avez aucun prospect dans votre liste</h1>
                        <?php endif;?>
                    </form>
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>