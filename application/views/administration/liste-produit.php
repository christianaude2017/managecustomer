<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Gestion des produits</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() ?>">Accueil</a>
            </li>
            <li class="active">
                <strong>Produits</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php //var_dump($produits) ?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Opération sur les produits</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-success btn-rounded" href="<?php echo site_url(); ?>/welcome/ajoutproduit">
                                <i class="fa fa-plus"> </i>&nbsp;&nbsp;Ajouter un produit
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-primary btn-rounded">
                                <i class="fa fa-edit"> </i>&nbsp;&nbsp; Modifier le produit
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-primary btn-rounded">
                                <i class="fa fa-eye"> </i>&nbsp;&nbsp; Voir détails du produit
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-danger btn-rounded">
                                <i class="fa fa-trash"> </i>&nbsp;&nbsp; Supprimer le produit
                            </a>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>LISTE DES PRODUITS</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="table-liste-role" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nom du produit</th>
                                    <th>Montant du produit</th>
                                    <th>Commission sur produit</th>
                                    <th>Activation</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($produits as $key): ?>
                                    <tr class="gradeX">

                                        <td><?php echo $key['idproduits'] ?></td>
                                        <td><?php echo $key['Libelleproduits'] ?></td>
                                        <td class="money"><?php echo $key['Montantproduits'] ?></td>
                                        <td><?php echo $key['pourcentage_commission']." %" ?></td>
                                        <td>
                                            <div class="switch">
                                                <div class="onoffswitch">
                                                    <input type="checkbox" <?php if($key['top_actif']==1) echo 'checked'?>  class="onoffswitch-checkbox" id="<?php echo $key['idproduits'] ?>" onchange="setactive(<?php echo $key['idproduits']?>,<?php echo $key['top_actif']?>)" > 
                                                    <label class="onoffswitch-label" for="<?php echo $key['idproduits'] ?>">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="center">
                                            <a href="modifierproduit?d=<?php echo base64_encode($key['idproduits']) ?>" class="btn btn-danger" id="<?php echo $key['idproduits'] ?>" ><span class="fa fa-edit"></span></a>
                                           
                                        </td>

                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>