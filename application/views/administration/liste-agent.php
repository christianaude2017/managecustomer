<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Gestion des agents</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() ?>">Accueil</a>
            </li>
            <li class="active">
                <strong>Rôles</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Opération sur les agents</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-success btn-rounded" href="<?php echo site_url(); ?>/welcome/ajoutagent">
                                <i class="fa fa-plus"> </i>&nbsp;&nbsp;Ajouter un agent
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-primary btn-rounded">
                                <i class="fa fa-edit"> </i>&nbsp;&nbsp; Modifier l'agent
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-primary btn-rounded">
                                <i class="fa fa-eye"> </i>&nbsp;&nbsp; Voir détails de l'agent
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-danger btn-rounded">
                                <i class="fa fa-trash"> </i>&nbsp;&nbsp; Supprimer l'agent
                            </a>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>LISTE DES AGENTS</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php //var_dump($agents)?>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="table-liste-role" >
                            <thead>
                                <tr>
                                    <th>Nom & Prenom Agent</th>
                                    <th>Sexe</th>
                                    <th>ville</th>
                                    <th>Commune</th>
                                    <th>Telephone</th>
                                    <th>Email</th>
                                    <th>actions</th>
                                    <th>statistiques</th>
                          
                                </tr>
                            </thead>
                            <tbody>
                                
                                <?php foreach ($agents as $agent): ?>
                                
                                <tr class="gradeX" id="tableau">
                                    <td class="center"><?php echo $agent['NomPersonne'].' '.$agent['PrenomPersonne']?></td>
                                    <td class="center"><?php echo $agent['sexe'] ?></td>
                                    <td class="center"><?php echo $agent['Libelleville'] ?></td>
                                    <td class="center"><?php echo $agent['Libellecommune'] ?></td>
                                    <td class="center"><?php echo $agent['contact1'].' / '.$agent['contact2'] ?></td>
                                    <td class="center"><?php echo $agent['email'] ?></td>
                                    <td class="center">
                                        <?php if ($agent['idPersonne'] == $_SESSION['idUser']): ?>
                                            <a href="modifieragent?d=<?php echo base64_encode($agent['idPersonne'])?>" class="btn btn-danger" id="<?php echo $agent['idPersonne']?>" ><span class="fa fa-edit"></span></a>
                                            <a href="#" class="btn btn-success" onclick="deleteAgent(<?php echo $agent['idPersonne']?>)" id="<?php echo $agent['idPersonne']?>" ><span class="fa fa-archive"></span></a>
                                         <?php endif; ?>
                                    </td>
                                    <td class="center">
                                        <a href="<?php if ($agent['idPersonne'] == $_SESSION['idUser']) echo site_url().'/welcome/statistique' ?>">
                                            <i class="fa fa-eye"></i>&nbsp;&nbsp; Consulter
                                        </a>
                                    </td>
                                </tr>
                                
                                <?php endforeach ?>
                            </tbody>
                            
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>