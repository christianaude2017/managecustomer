<!DOCTYPE html>
<html>


    <!-- Mirrored from webapplayers.com/inspinia_admin-v2.7.1/login_two_columns.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jan 2018 12:55:00 GMT -->
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>INSPINIA | Login 2</title>

        <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">

        <!-- Toastr style -->
        <link href="<?php echo base_url() ?>assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <!-- confirm css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <!--Bootstrap Validator [ OPTIONAL ]-->
        <link href="<?php echo base_url() ?>/assets/Admin/plugins/bootstrap-validator/bootstrapValidator.min.css" rel="stylesheet">


    </head>

    <body class="gray-bg">

        <div class="loginColumns animated fadeInDown">
            <div class="row">

                <div class="col-md-6">
                    <h2 class="font-bold">CRÉATION DE COMPTE</h2>
                    <p>
                        QUELQUES MINUTES SEULEMENT SUFFISENT
                    </p>

                    <p>
                        1. Enregistrez les informations relatives à votre entreprise. Vous pouvez les compléter plus tard
                    </p>

                    <p>
                        2. Entrez les identifiants de connexion qui serviront pour votre accès à l'espace de votre entreprise
                    </p>

                    <p>
                        <small>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</small>
                    </p>

                </div>
                <div class="col-md-6">
                    <div class="ibox-content">
                        <form class="m-t" role="form" action="http://localhost/apimanagecustomer/public/inscription" id="form-register">
                            <div class="form-group">
                                <input type="text" class="form-control" name="denomination" placeholder="Dénomination de la société" required="">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="nomUser" placeholder="Nom" required="">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="prenomUser" placeholder="Prenom" required="">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="loginUser" placeholder="login" required="">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="emailUser" placeholder="email" required="">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="motdepasse" placeholder="saisir mot de passe" required="">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="cmotdepasse" placeholder="confirmez mot de passe" required="">
                            </div>
                            <button type="submit" name="enregistrer" class="btn btn-primary block full-width m-b">Créer le compte</button>

                            <p class="text-muted text-center">
                                <small>Vous avez déjà un compte ?</small>
                            </p>
                            <a class="btn btn-sm btn-white btn-block" href="<?php echo site_url(); ?>/welcome/connexion">Se connecter</a>
                        </form>

                    </div>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-6">
                    Copyright Example Company
                </div>
                <div class="col-md-6 text-right">
                    <small>© 2014-2015</small>
                </div>
            </div>
        </div>
        <script src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>/assets/js/plugins/bootstrap-validator/bootstrapValidator.min.js"></script>

        <!-- Toastr -->
        <script src="<?php echo base_url() ?>assets/js/plugins/toastr/toastr.min.js"></script>
        
        <!-- confirm js-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

        <script src="<?php echo base_url() ?>/assets/js/jsScript/register.js"></script>
    </body>


    <!-- Mirrored from webapplayers.com/inspinia_admin-v2.7.1/login_two_columns.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jan 2018 12:55:00 GMT -->
</html>


