<!DOCTYPE html>
<html>


    <!-- Mirrored from webapplayers.com/inspinia_admin-v2.7.1/login_two_columns.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jan 2018 12:55:00 GMT -->
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>INSPINIA | Login 2</title>

        <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">


        <!-- Toastr style -->
        <link href="<?php echo base_url() ?>assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">

        <!-- confirm css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <!--Bootstrap Validator [ OPTIONAL ]-->
        <link href="<?php echo base_url() ?>/assets/Admin/plugins/bootstrap-validator/bootstrapValidator.min.css" rel="stylesheet">


    </head>

    <body class="gray-bg">
        <?php //var_dump($statut)?>
        <div class="loginColumns animated fadeInDown">
            <div class="row">

                <div class="col-md-6">
                    <h2 class="font-bold">Connectez-vous</h2>

                    <p>
                        Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.
                    </p>

                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                    </p>

                    <p>
                        When an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    </p>

                    <p>
                        <small>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</small>
                    </p>

                </div>
                <div class="col-md-6">
                    <div class="ibox-content">
                        <form class="m-t" role="form" method="POST" action="http://localhost/apimanagecustomer/public/login" id="form-login">
                            <div class="form-group">
                                <input type="email" class="form-control" name="loginUser" placeholder="login" required="">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="motdepasse" placeholder="Mot de passe" required="">
                            </div>
                            <button type="submit" class="btn btn-primary block full-width m-b" id="btnLogin">Se connecter</button>
                        </form>
                        <a href="#">
                            <small>Mot de passe oublié ?</small>
                        </a>

                        <p class="text-muted text-center">
                            <small>Vous n'avez pas de compte ?</small>
                        </p>
                        <a class="btn btn-sm btn-white btn-block" href="<?php echo site_url(); ?>/welcome/register">Créer un compte</a>

                        <p class="m-t">
                            <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small>
                        </p>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-6">
                    Copyright Example Company
                </div>
                <div class="col-md-6 text-right">
                    <small>© 2014-2015</small>
                </div>
            </div>
        </div>

        <script src="<?php echo base_url() ?>assets/js/jquery-3.1.1.min.js"></script>
        <script src="<?php echo base_url() ?>/assets/js/plugins/bootstrap-validator/bootstrapValidator.min.js"></script>

        <!-- confirm js-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

        <!-- Toastr -->
        <script src="<?php echo base_url() ?>assets/js/plugins/toastr/toastr.min.js"></script>

        <script type="text/javascript">
            var redirectionAccueil = "<?php echo site_url(); ?>/welcome/afficherVue";
            
        </script>

        <script src="<?php echo base_url() ?>/assets/js/jsScript/login.js"></script>
    </body>


    <!-- Mirrored from webapplayers.com/inspinia_admin-v2.7.1/login_two_columns.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jan 2018 12:55:00 GMT -->
</html>

