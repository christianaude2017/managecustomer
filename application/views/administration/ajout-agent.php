<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Gestion des agents</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() ?>">Accueil</a>
            </li>
            <li>
                <a href="<?php echo site_url() ?>/welcome/listeagent">Agents</a>
            </li>
            <li class="active">
                <strong>Ajout agent</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3>Formulaire d'ajout de agents <small></small></h3>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                
                <div class="ibox-content">
                    <form method="post" action="http://localhost/apimanagecustomer/public/addagent" class="form-horizontal" id="form-crea-agent">

                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <h5 class="col-sm-3">INFOS DE L'AGENT</h5>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                      
                        <div class="form-group"><label class="col-sm-3 control-label"></label>
                            <div class="col-sm-8"><input  type="hidden" name="idpersonne" id="idpersonne" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label"></label>
                            <div class="col-sm-8"><input  type="hidden" name="societe" value="<?php echo $_SESSION['idsociete']?>" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">NOM</label>
                            <div class="col-sm-8"><input type="text" name="nomAgent" class="form-control"></div>
                        </div>
                        
                        <div class="form-group"><label class="col-sm-3 control-label">PRÉNOMS</label>
                            <div class="col-sm-8"><input type="text" value="" name="prenomAgent" class="form-control"></div>
                        </div>
                        
                        <div class="form-group"><label class="col-sm-3 control-label">DATE DE NAISSANCE</label>
                            <div class="col-sm-4">
                                <div class="">
                                    <!--<span class="input-group-addon">
                                        <i class=""></i> fa fa-calendar input-group date
                                    </span>--><input type="date" name="datenaissAgent" class="form-control">
                                </div>
                            </div>
                        </div>
                       
                        <div class="form-group"><label class="col-sm-3 control-label">SEXE</label>

                            <div class="col-sm-8">
                                <div class="i-checks">
                                    <label class="checkbox-inline"> 
                                        <input type="radio" value="homme" name="sexeAgent" id="inlineCheckbox1"> Homme
                                    </label>
                                </div>
                                <div class="i-checks">
                                    <label class="checkbox-inline">
                                        <input type="radio" value="femme" name="sexeAgent" id="inlineCheckbox2"> Femme 
                                    </label> 
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">PAYS D'ORIGINE</label>
                            <div class="col-sm-3">
                                <select class="chosen-select" name="paysAgent">
                                    <option value="3">Mali</option>
                                    <option value="2">Burkina Faso</option>
                                    <option value="1">Côte d'Ioire</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">VILLE</label>
                            <div class="col-sm-3">
                                <select class="chosen-select" name="villehabitationAgent">
                                    <option value="1">Abidjan</option>
                                    <option value="2">Abengourou</option>
                                    <option value="3">Adiaké</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">COMMUNE</label>
                            <div class="col-sm-3">
                                <select class="chosen-select" name="communehabitationAgent">
                                    <option value="1">Marcory</option>
                                    <option value="2">Cocody</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">CONTACT 1</label>
                            <div class="col-sm-3"><input type="text" name="contact1Agent" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">CONTACT 2</label>
                            <div class="col-sm-3"><input type="text" name="contact2Agent" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">EMAIL</label>
                            <div class="col-sm-5"><input type="email" name="emailAgent" class="form-control"></div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <h5 class="col-sm-3">INFOS DU RESPONSABLE</h5>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            
                            <label class="col-sm-3 control-label">RESPONSABLE</label>
                            
                            <div class="col-sm-8">
                                <select data-placeholder="Choose a Country..." name="responsableAgent" class="chosen-select">
                                    <option value="">Selectionnez le responsbale de cet agent</option>
                                </select>
                                <span class="help-block m-b-none">Sélectionner le responsable s'il y'a lieu</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-3">
                                <button class="btn btn-white" type="reset">Vider les champs</button>
                                <button class="btn btn-primary" type="submit">Enregistrer</button>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>