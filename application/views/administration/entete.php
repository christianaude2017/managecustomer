<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.7.1
*
-->

<!DOCTYPE html>
<html>


    <!-- Mirrored from webapplayers.com/inspinia_admin-v2.7.1/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jan 2018 12:50:22 GMT -->
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>INSPINIA | Dashboard</title>

        <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Toastr style -->
        <link href="<?php echo base_url() ?>assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <!-- Gritter -->
        <link href="<?php echo base_url() ?>assets/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

        <link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
        <!-- c3 Charts -->
        <link href="<?php echo base_url() ?>assets/css/plugins/c3/c3.min.css" rel="stylesheet">
        <!-- Morris -->
        <link href="<?php echo base_url() ?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
        
        <!-- Switchery -->
        <link href="<?php echo base_url() ?>assets/css/plugins/switchery/switchery.css" rel="stylesheet">

        <!-- confirm css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <!-- jquery modal-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
        <!--Bootstrap Validator [ OPTIONAL ]-->
        <link href="<?php echo base_url() ?>/assets/Admin/plugins/bootstrap-validator/bootstrapValidator.min.css" rel="stylesheet">


    </head>
    
    
    <body>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element"> <span>
                                    <img alt="image" class="img-circle" src="<?php echo base_url() ?>assets/img/profile_small.jpg" />
                                </span>
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    <span class="clear"> <span class="block m-t-xs"> 
                                            <strong class="font-bold">
                                                <?php if(isset($_SESSION))echo $_SESSION['name']?>
                                            </strong>
                                        </span> <span class="text-muted text-xs block"><?php if(isset($_SESSION))echo $_SESSION['societeLibelle']?><b class="caret"></b></span>
                                    </span> </a>
                                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li><a href="profile.html">Profile</a></li>
                                    <li><a href="contacts.html">Contacts</a></li>
                                    <li><a href="mailbox.html">Mailbox</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<?php echo site_url(); ?>/welcome/endSession">Déconnexion</a></li>
                                </ul>
                            </div>
                            <div class="logo-element">
                                MC+
                            </div>
                        </li>
                        <li class="active">
                            <a href="<?php echo site_url() ?>/welcome/afficherVue"><i class="fa fa-home"></i><span class="nav-label">accueil</span></a>                            
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-diamond"></i> <span class="nav-label">Société</span></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Agents </span>
                                <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a disable="" href="<?php echo site_url() ?>/welcome/listeresponsable"><i class="fa fa-filter"></i> Responsables</a></li>
                                <li><a disable="" href="<?php echo site_url() ?>/welcome/listeagent"><i class="fa fa-filter"></i> Agents</a></li>

                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-cubes"></i> <span class="nav-label">Produits</span>
                                <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="<?php echo site_url() ?>/welcome/listeproduit"><i class="fa fa-filter"></i>  Produits</a></li>
                                
                                <li><a href="<?php echo site_url() ?>/welcome/listeprime"><i class="fa fa-filter"></i> Primes</a></li>

                            </ul>
                        </li>                    
                        <li>
                            <a href="<?php echo site_url() ?>/welcome/listeprospection">
                                <i class="fa fa-clipboard"></i> 
                                <span class="nav-label">Prospections</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url() ?>/welcome/listeRendezVous">
                                <i class="fa fa-clock-o"></i> 
                                <span class="nav-label">Rendez-vous</span>
                            </a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o"></i> <span class="nav-label">Contrats</span>
                                <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="<?php echo site_url() ?>/welcome/listeclient"><i class="fa fa-filter"></i> Clients</a></li>
                                <li><a href="<?php echo site_url() ?>/welcome/listecontrat"><i class="fa fa-filter"></i> Contrats</a></li>

                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo site_url() ?>/welcome/listepaiement"><i class="fa fa-money"></i> <span class="nav-label">Paiements</span></a>
                        </li>
                        
                        <li>
                            <a href="#"><i class="fa fa-area-chart"></i> <span class="nav-label">Statistiques</span>
                                <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">             
                                <li><a href="<?php echo site_url() ?>/welcome/tableauRecap"><i class="fa fa-bar-chart"></i>Tableau recapitulatif</a></li>
                                <li><a href="<?php echo site_url() ?>/welcome/statistique"><i class="fa fa-bar-chart"></i>Statistique par agent</a></li>
                                <li><a href="<?php echo site_url() ?>/welcome/statistique_entreprise"><i class="fa fa-bars"></i>Statistique globale</a></li>
                                
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-cogs"></i> <span class="nav-label">Parametrage</span>
                                <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">             
                                <li><a href="<?php echo site_url() ?>/welcome/listerole"><i class="fa fa-filter"></i> Rôles</a></li>
                                <li><a href="<?php echo site_url() ?>/welcome/listemodepaiement"><i class="fa fa-filter"></i>  Modes de paiement</a></li>
                                <li><a href="<?php echo site_url() ?>/welcome/listecategorieproduit"><i class="fa fa-filter"></i>  Catégorie produits</a></li>
                            </ul>
                        </li>
                </div>
            </nav>
            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
                            </a>
                            <form role="search" class="navbar-form-custom" action="http://webapplayers.com/inspinia_admin-v2.7.1/search_results.html">
                                <div class="form-group">
                                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search"
                                           id="top-search">
                                </div>
                            </form>
                        </div>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <span class="m-r-sm text-muted welcome-message">Welcome to MANAGE CUSTOMER</span>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                    <i class="fa fa-envelope"></i> <span class="label label-warning">16</span>
                                </a>
                                <ul class="dropdown-menu dropdown-messages">
                                    <li>
                                        <div class="dropdown-messages-box">
                                            <a href="profile.html" class="pull-left">
                                                <img alt="image" class="img-circle" src="<?php echo base_url() ?>assets/img/a7.jpg">
                                            </a>
                                            <div class="media-body">
                                                <small class="pull-right">46h ago</small>
                                                <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>.
                                                <br>
                                                <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <div class="dropdown-messages-box">
                                            <a href="profile.html" class="pull-left">
                                                <img alt="image" class="img-circle" src="<?php echo base_url() ?>assets/img/a4.jpg">
                                            </a>
                                            <div class="media-body ">
                                                <small class="pull-right text-navy">5h ago</small>
                                                <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica
                                                    Smith</strong>. <br>
                                                <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <div class="dropdown-messages-box">
                                            <a href="profile.html" class="pull-left">
                                                <img alt="image" class="img-circle" src="<?php echo base_url() ?>assets/img/profile.jpg">
                                            </a>
                                            <div class="media-body ">
                                                <small class="pull-right">23h ago</small>
                                                <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                                <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <div class="text-center link-block">
                                            <a href="mailbox.html">
                                                <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                    <i class="fa fa-bell"></i> <span class="label label-primary">8</span>
                                </a>
                                <ul class="dropdown-menu dropdown-alerts">
                                    <li>
                                        <a href="mailbox.html">
                                            <div>
                                                <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                                <span class="pull-right text-muted small">4 minutes ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="profile.html">
                                            <div>
                                                <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                                <span class="pull-right text-muted small">12 minutes ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="grid_options.html">
                                            <div>
                                                <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                                <span class="pull-right text-muted small">4 minutes ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <div class="text-center link-block">
                                            <a href="notifications.html">
                                                <strong>See All Alerts</strong>
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </li>


                            <li>
                                <a href="<?php echo site_url(); ?>/welcome/endSession">
                                    <i class="fa fa-sign-out"></i> Se déconnecter
                                </a>
                            </li>
                            <li>
                                <a class="right-sidebar-toggle">
                                    <i class="fa fa-tasks"></i>
                                </a>
                            </li>
                        </ul>

                    </nav>
                </div>