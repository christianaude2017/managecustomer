<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Gestion des clients</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() ?>">Accueil</a>
            </li>
            <li class="active">
                <strong>Clients</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Opération sur les clients</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-success btn-rounded" href="<?php echo site_url(); ?>/welcome/ajoutclient">
                                <i class="fa fa-plus"> </i>&nbsp;&nbsp;Ajouter un client
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-primary btn-rounded">
                                <i class="fa fa-edit"> </i>&nbsp;&nbsp; Modifier le client
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-primary btn-rounded">
                                <i class="fa fa-eye"> </i>&nbsp;&nbsp; Voir détails du client
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <a class="btn btn-danger btn-rounded">
                                <i class="fa fa-trash"> </i>&nbsp;&nbsp; Supprimer le client
                            </a>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>LISTE DES AGENTS</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="table-liste-role" >
                            <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>Prenom</th>
                                    <th>Age</th>
                                    <th>Entreprise</th>
                                    <th>Date d'ajout</th>
                                    <th>Agent souscripteur</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php //var_dump($_SESSION)?>
                                <?php foreach($clients as $client):?>
                                <tr class="gradeX">
                                    <td><?php echo $client['NomClient']?></td>
                                    <td><?php echo $client['PrenomClient']?></td>
                                    <td><?php echo $client['ageClient']?></td>
                                    <td><?php echo $client['societeClient']?></td>
                                    <td class="center"><?php echo $client['dateAjout']?></td>
                                    <td><?php echo $client['NomPersonne']." ".$client['PrenomPersonne']?></td>
                                    <td class="center">
                                            <?php if ($client['idagent'] == $_SESSION['idUser']): ?>
                                            <a href="modifierClient?d=<?php echo base64_encode($client['idClient']) ?>" class="btn btn-danger" title="modifier la prospection" id="" ><span class="fa fa-edit"></span></a>
                                            <a href="#" class="btn btn-success" title="supprimer la prospection" onclick="deleteClient(<?php echo $client['idClient'] ?>);" id="<?php echo $client['idClient'] ?>" ><span class="fa fa-archive"></span></a>
                                            <?php endif; ?>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>