<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Gestion des prospections</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() ?>">Accueil</a>
            </li>
            <li>
                <a href="<?php echo site_url() ?>/welcome/listeprospection">Prospections</a>
            </li>
            <li class="active">
                <strong>Ajout prospections</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulaire d'ajout de prospections <small></small></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <form method="post" class="form-horizontal" action="http://localhost/apimanagecustomer/public/addProspection" id="form-prospect">
                        <!-- <div class="form-group">
                            <label class="col-sm-3 control-label">AGENT</label>
                            <div class="col-sm-8">
                                <select data-placeholder="Selectionnez la catégoriie prospection..." class="chosen-select" name="agentProspect">
                                    <?php foreach ($agents as $agent): ?>
                                    <option value="<?php echo $agent['idPersonne'] ?>" selected="<?php echo $_SESSION['idUser']?>"><?php echo ucfirst($agent['NomPersonne']) . ' ' . ucfirst($agent['PrenomPersonne']) ?></option>
                                    <?php endforeach; ?>
                                </select>

                            </div>
                        </div>-->
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label"></label>
                            <div class="col-sm-8"><input type="hidden" value="<?php echo $_SESSION['idUser'] ?>" name="agentProspect" class="form-control"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">PRODUIT</label>
                            <div class="col-sm-8">

                                <select data-placeholder="Selectionnez la catégoriie prospection..." class="chosen-select" name="produitProspect">
                                    <?php foreach ($produits as $produit): ?>
                                        <option value="<?php echo $produit['idproduits'] ?>"><?php echo ucfirst($produit['Libelleproduits']) ?></option>
                                    <?php endforeach; ?>
                                </select>

                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label"></label>
                            <div class="col-sm-8"><input type="hidden" value="<?php echo $_SESSION['idsociete'] ?>" name="societe" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">NOM PROSPECT</label>
                            <div class="col-sm-8"><input type="text" name="nomProspect" class="form-control"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">PRÉNOMS PROSPECT</label>
                            <div class="col-sm-8"><input type="text" name="prenomProspect" class="form-control"></div>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">SOCIÉTÉ PROSPECT</label>
                            <div class="col-sm-8"><input type="text" name="societeProspect" class="form-control"></div>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">ADRESSE SOCIETE</label>
                            <div class="col-sm-8"><input type="text" name="adresseProspect" class="form-control"></div>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">FONCTION PROSPECT</label>
                            <div class="col-sm-8"><input type="text" name="fonctionProspect" class="form-control"></div>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">TEL PROSPECT</label>
                            <div class="col-sm-8"><input type="text" name="telProspect" class="form-control"></div>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">EMAIL PROSPECT</label>
                            <div class="col-sm-8"><input type="email" name="emailProspect" class="form-control"></div>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">DATE PRISE RENDEZ-VOUS</label>
                            <div class="col-sm-8"><input type="date" name="dateProspect" class="form-control"></div>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">DATE RENDEZ-VOUS</label>
                            <div class="col-sm-8"><input type="date" name="dateRDV" class="form-control"></div>                        
                        </div>
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-3">
                                <button class="btn btn-white" type="reset">Vider les champs</button>
                                <button class="btn btn-primary" type="submit">Enregistrer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>