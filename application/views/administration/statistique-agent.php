<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Statistiques</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo site_url() ?>/welcome/afficherVue">Home</a>
            </li>
            <li>
                <a href="<?php echo site_url() ?>/welcome/listeagent">Agents</a>
            </li>
            <li class="active">
                <strong>Statistiques</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="form-group">
            <label class="col-sm-3 control-label">Pour quel agent voulez-vous faire ?</label>
            <div class="col-sm-8">
                <select data-placeholder="Selectionnez le rôle de cet client..." class="chosen-select input-lg" id="idAgent">
                    <?php foreach($agents as $agent):?>
                    <option value="<?php echo $agent['idPersonne']?>"><?php echo $agent['NomPersonne']." ".$agent['PrenomPersonne']?></option>
                    <?php endforeach;?>
                </select>

            </div>
        </div>
    </div>
    <div class="row"></div>
    <br><br><br>
    <div class="row">
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Statistiques périodiques</h5>
                    <h4 class="text-right">
                        du <input type="date" id="datedebut"> au <input type="date" id="datefin"> <button id="search"><span class="fa fa-search"></span></button>
                    </h4>
                </div>
                <div class="ibox-content">
                    <div>
                        <canvas id="barChart" height="140"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Productivité de l'agent</h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <div id="gauge"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Statistiques Mensuelles
                        <small>Mesure la productivité de l'agent pour l'année en cours</small>
                    </h5>
                    <h4 class="text-right">
                        Année 2019
                    </h4>
                </div>
                <div class="ibox-content">
                    <div>
                        <canvas id="lineChart" height="100"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
