<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Gestion des produits</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() ?>">Accueil</a>
            </li>
            <li>
                <a href="<?php echo site_url() ?>/welcome/listeproduit">Produits</a>
            </li>
            <li class="active">
                <strong>Ajout produits</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php //var_dump($data) ?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulaire d'ajout de produits <small></small></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" action="http://localhost/apimanagecustomer/public/updateProduit" class="form-horizontal" id="form-add-produit">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">CATÉGORIE PRODUIT</label>
                            <div class="col-sm-6">
                                <select data-placeholder="Selectionnez la catégoriie produit..." class="chosen-select" name="categorie">
                                    <?php foreach ($listCateg as $key): ?>
                                              <option value="<?php echo $key['idcatgorieproduit']?>" <?php if($key['idcatgorieproduit']==$data->idcatgorieproduit) echo 'selected' ?> >
                                              <?php echo $key['Libellecatgorieproduit']?>
                                          </option>
                                    <?php endforeach ?>
                                </select>    
                            </div>
                            <div class="col-sm-1"><a class="btn btn-primary" href="#ex1" rel="modal:open" title="nouvelle catégorie"><span class="fa fa-plus"></span></a></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">LIBÉLLÉ</label>
                            <div class="col-sm-6"><input type="text" class="form-control" name="libelleproduit" value="<?php echo $data->Libelleproduits ?>"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">MONTANT</label>
                            <div class="col-sm-4"><input type="text" class="form-control money" name="montantproduit" value="<?php echo $data->Montantproduits ?>">
                                <span class="help-block m-b-none">Saisir le montant du produit (FCFA)</span>
                            </div>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">% DE LA COMMISSION SUR LE PRODUIT</label>
                            <div class="col-sm-4"><input type="text" class="form-control" name="commission" value="<?php echo $data->pourcentage_commission ?>">
                                <span class="help-block m-b-none">Saisir le % de la commission</span>
                            </div>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div id="ex1" class="modal">
                            <input class="form-control" type="hidden" value="<?php echo $data->idproduits?>" name="produit" placeholder="entrez le nom de la catégorie">
                            <input class="form-control" type="text" id="categorie" placeholder="entrez le nom de la catégorie">
                            <input class="form-control" type="hidden" id="idsociete" name="idsociete" value="<?php echo $_SESSION['idsociete'] ?>">
                            <br>
                            <div>
                                <a href="#" class="btn btn-danger" rel="modal:close">Close</a>
                                <a class="btn btn-success" rel="modal:close" onclick="addCategorie()">valider</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-3">
                                <button class="btn btn-white" type="reset">Vider les champs</button>
                                <button class="btn btn-primary" type="submit">Enregistrer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var urlSetCategorie = <?php echo "http://localhost/apimanagecustomer/public/addCategorie" ?>
</script>