<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Gestion des paiements</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url() ?>">Accueil</a>
            </li>
            <li>
                <a href="<?php echo site_url() ?>/welcome/listepaiement">Paiements</a>
            </li>
            <li class="active">
                <strong>Ajout paiements</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulaire d'ajout de paiements <small></small></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="get" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">CONTRAT</label>
                            <div class="col-sm-8">
                                <select data-placeholder="Selectionnez le contrat..." class="chosen-select">
                                    <option value="">Aucun</option>
                                    <option value="United States">United States</option>
                                    <option value="United Kingdom">United Kingdom</option>
                                    <option value="Afghanistan">Afghanistan</option>
                                    <option value="Aland Islands">Aland Islands</option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="American Samoa">American Samoa</option>
                                </select>
                                <span class="help-block m-b-none">Sélectionner le contrat objet du paiement</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">MODE DE PAIEMENT</label>
                            <div class="col-sm-8">
                                <select data-placeholder="Selectionnez le mode de paiement..." class="chosen-select">
                                    <option value="">Aucun</option>
                                    <option value="United States">United States</option>
                                    <option value="United Kingdom">United Kingdom</option>
                                    <option value="Afghanistan">Afghanistan</option>
                                    <option value="Aland Islands">Aland Islands</option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="American Samoa">American Samoa</option>
                                </select>
                                <span class="help-block m-b-none">Sélectionner le mode de paiement</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">MOIS (ÉCHÉANCE)</label>
                            <div class="col-sm-8">
                                <select data-placeholder="Selectionnez l paiement..." class="chosen-select">
                                    <option value="">Aucun</option>
                                    <option value="United States">Janvier</option>
                                    <option value="United Kingdom">Février</option>
                                    <option value="Afghanistan">Mars</option>
                                    <option value="Aland Islands">Avril</option>
                                    <option value="Albania">Mai</option>
                                    <option value="Algeria">Juin</option>
                                    <option value="American Samoa">Juillet</option>
                                </select>
                                <span class="help-block m-b-none">Sélectionner le mois lié au paiement</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">DATE DE PAIEMENT</label>
                            <div class="col-sm-8">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">MONTANT DU PAIEMENT</label>
                            <div class="col-sm-8"><input type="number" class="form-control">
                                <span class="help-block m-b-none">Saisir le montant du paiement (FCFA)</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-3 control-label">NUMERO CHEQUE</label>
                            <div class="col-sm-8"><input type="text" class="form-control"></div>                           
                        </div>
                        <div class="hr-line-dashed"></div>
                        
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-3">
                                <button class="btn btn-white" type="reset">Vider les champs</button>
                                <button class="btn btn-primary" type="submit">Enregistrer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>