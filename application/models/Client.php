<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Client
 *
 * @author DJAMARA
 */
class Client extends CI_Model {

    //put your code here

    public function __construct() {
        parent::__construct();
    }

    public function getAllClient() {

        $sql = 'select  * , FLOOR(DATEDIFF(NOW(),client.DateNaissClient)/360) 
                as ageClient from client,personne where client.idagent = personne.idPersonne and client.top_actif = 1';
        
        $exeq = $this->db->query($sql);
        $result = $exeq->result_array();
        return $result;
    }
    
    public function getAllClientBySoc($idSociete) {

        $sql = 'select  * , FLOOR(DATEDIFF(NOW(),client.DateNaissClient)/360) 
                as ageClient from client,personne where client.idagent = personne.idPersonne and client.top_actif = 1
                and personne.societe_idsociete = ?';
        
        $exeq = $this->db->query($sql,array($idSociete));
        $result = $exeq->result_array();
        return $result;
    }
    
    public function getClientById($idClient) {

        $sql = 'select  * , FLOOR(DATEDIFF(NOW(),client.DateNaissClient)/360) 
                as ageClient from client,personne where client.idagent = personne.idPersonne
                and idClient = ? and client.top_actif = 1';
        
        $exeq = $this->db->query($sql,array($idClient));
        $result = $exeq->result_array();
        return $result;
    }

    public function getAllClientByAgent($idAgent,$idsociete) {

        $sql = "select  * , FLOOR(DATEDIFF(NOW(),client.DateNaissClient)/360) 
                as ageClient from client,personne where client.idagent = personne.idPersonne and client.top_actif = 1
                and client.idagent = ? and personne.societe_idsociete = ?";
        $exeq = $this->db->query($sql, array($idAgent,$idsociete));
        $result = $exeq->result_array();
        return $result;
    }

}
