<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Agent
 *
 * @author DJAMARA
 */
class Agent extends CI_Model {
    //put your code here
    
    public function __construct() {
        parent::__construct();
        
    }
    public function getAllAgent(){
            
        $sql = "select * from personne,ville,commune 
                where personne.ville_idville = ville.idville
                and personne.commune_idcommune = commune.idcommune
                and rolePersonne_idrolePersonne = 2 and top_actif = 1 order by Nompersonne";
        $exeq = $this->db->query($sql);
        $result = $exeq->result_array();
        return $result;
            
    }
    
    public function getAllAgentbySociete($idsociete){
            
        $sql = "select * from personne,ville,commune
                where personne.ville_idville = ville.idville
                and personne.commune_idcommune = commune.idcommune
                and rolePersonne_idrolePersonne = 2 and personne.societe_idsociete = ?
                and top_actif = 1
                
                order by Nompersonne";
        $exeq = $this->db->query($sql,array($idsociete));
        $result = $exeq->result_array();
        return $result;
            
    }
    
    public function getAllResponsable(){
            
        $sql = "select * from personne,ville,commune 
                where personne.ville_idville = ville.idville
                and personne.commune_idcommune = commune.idcommune
                and rolePersonne_idrolePersonne = 2 and top_actif = 1 and top_respo = 1 order by Nompersonne";
        $exeq = $this->db->query($sql);
        $result = $exeq->result_array();
        return $result;
            
    }
    public function getAllNonResponsable(){
            
        $sql = "select * from personne,ville,commune 
                where personne.ville_idville = ville.idville
                and personne.commune_idcommune = commune.idcommune
                and rolePersonne_idrolePersonne = 2 and top_actif = 1 and top_respo = 0 order by Nompersonne";
        $exeq = $this->db->query($sql);
        $result = $exeq->result_array();
        return $result;
            
    }
    
    public function getAgentByRespo($idRespo){
        
        $sql = "SELECT responsable_agent.Personne_idAgent 
            FROM `responsable_agent` WHERE responsable_agent.Personne_idResponsable = ? ";
        $exeq = $this->db->query($sql, array($idRespo));
        
        $result = $exeq->result();
        return $result;
    }
    
    public function getAgentById($idAgent){
            
        $sql = "select * from personne,ville,commune 
                where personne.ville_idville = ville.idville
                and personne.commune_idcommune = commune.idcommune
                and rolePersonne_idrolePersonne = 2 and idPersonne = ?";
        $exeq = $this->db->query($sql, array($idAgent));
        $result = $exeq->result();
        return $result;
            
    }
    
    public function getUserById($idAgent){
            
        $sql = "select * from personne,compte,societe
                where personne.idPersonne = compte.idpersonne 
                and personne.idPersonne = ? and personne.societe_idsociete = societe.idsociete";
        $exeq = $this->db->query($sql, array($idAgent));
        $result = $exeq->result();
        return $result;
            
    }
}
