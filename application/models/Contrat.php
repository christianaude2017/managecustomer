<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Contrat
 *
 * @author DJAMARA
 */
class Contrat extends CI_Model {

    //put your code here

    public function __construct() {
        parent::__construct();
    }

    public function getAllContratbySocieteByAgent($idsociete,$idagent) {

        $sql = "select * from contrats,personne,client,produits
                where contrats.idagent = personne.idPersonne
                and contrats.idclient = client.idClient
                and contrats.idproduit = produits.idproduits
                and personne.societe_idsociete= ? and personne.idPersonne = ? and produits.top_actif = 1
                
                order by Nompersonne";
        $exeq = $this->db->query($sql, array($idsociete,$idagent));
        $result = $exeq->result_array();
        return $result;
    }
    
    public function getAllContratbySociete($idsociete) {

        $sql = "select * from contrats,personne,client,produits
                where contrats.idagent = personne.idPersonne
                and contrats.idclient = client.idClient
                and contrats.idproduit = produits.idproduits
                and personne.societe_idsociete= ? and produits.top_actif = 1
                
                order by Nompersonne";
        $exeq = $this->db->query($sql, array($idsociete));
        $result = $exeq->result_array();
        return $result;
    }
    
    public function getAllContratbyIdContrat($idcontrat) {

        $sql = "select * from contrats,personne,client,produits
                where contrats.idagent = personne.idPersonne
                and contrats.idclient = client.idClient
                and contrats.idproduit = produits.idproduits
                and contrats.idcontrat= ? and produits.top_actif = 1";
        $exeq = $this->db->query($sql, array($idcontrat));
        $result = $exeq->result_array();
        $result = $result[0];
        return $result;
    }
    
    public function getModePaiementBySociete($idSociete){
            
        $sql = "select * from modepaiement 
                where societe_idsociete = ? and top_actif = 1";
        $exeq = $this->db->query($sql, array($idSociete));
        $result = $exeq->result_array();
        return $result;
            
    }

}
