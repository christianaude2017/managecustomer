<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Prospection
 *
 * @author DJAMARA
 */
class Prospection extends CI_Model {

    //put your code here

    public function __construct() {
        parent::__construct();
    }

    public function getAllprospectionBysoc($idsociete, $idagent) {

        $req = "select * from prospections,personne,produits,societe where
                prospections.agentprospection = personne.idPersonne 
                and prospections.produitprospection = produits.idproduits 
                and prospections.idsociete = societe.idsociete 
                and prospections.idsociete = ? and prospections.agentprospection = ? 
                and prospections.top_actif = 1
                order by dateprospection DESC";

        $exeq = $this->db->query($req, array($idsociete, $idagent));
        $result = $exeq->result_array();
        return $result;
    }

    public function getAllprospection($idsociete) {

        $req = "select * from prospections,personne,produits,societe where
                prospections.agentprospection = personne.idPersonne 
                and prospections.produitprospection = produits.idproduits 
                and prospections.idsociete = societe.idsociete 
                and prospections.idsociete = ?
                and prospections.top_actif = 1
                order by dateprospection DESC";

        $exeq = $this->db->query($req, array($idsociete));
        $result = $exeq->result_array();
        return $result;
    }

    public function getAllRendezVous($idsociete) {

        $req = "select DISTINCT(rdvprospect.idprospection) as id,rdvprospect.idsociete,
                prospections.nomprospection,prospections.prenomprospection,
                personne.NomPersonne,personne.PrenomPersonne,prospections.dateRDVprospection,
                (select count(rdvprospect.idrdvprospect) from rdvprospect where rdvprospect.idprospection = id) as nombre,
                (select  max(rdvprospect.daterdv) from rdvprospect where rdvprospect.idprospection = id ) lastrdv,
                prospections.*,personne.*
                from rdvprospect,prospections,personne
                where rdvprospect.idprospection = prospections.idprospection
                and personne.idPersonne = prospections.agentprospection
                
                and prospections.idsociete = ?";

        $exeq = $this->db->query($req, array($idsociete));
        $result = $exeq->result_array();
        return $result;
    }

    public function getAllRendezVousBysoc($idsociete, $idagent) {

        $req = "select DISTINCT(rdvprospect.idprospection) as id,rdvprospect.idsociete,
                prospections.nomprospection,prospections.prenomprospection,
                personne.NomPersonne,personne.PrenomPersonne,prospections.dateRDVprospection,
                (select count(rdvprospect.idrdvprospect) from rdvprospect where rdvprospect.idprospection = id) as nombre,
                (select  max(rdvprospect.daterdv) from rdvprospect where rdvprospect.idprospection = id ) lastrdv,
                prospections.*,personne.*
                from rdvprospect,prospections,personne
                where rdvprospect.idprospection = prospections.idprospection
                and personne.idPersonne = prospections.agentprospection
                and prospections.idsociete = ? and prospections.agentprospection = ?";

        $exeq = $this->db->query($req, array($idsociete, $idagent));
        $result = $exeq->result_array();
        return $result;
    }

    /*public function getProspectionById($idprospection) {

        $req = "select * 
                from prospections,rdvprospect
                where prospections.idprospection = rdvprospect.idprospection
                and rdvprospect.daterdv = (select max(rdvprospect.daterdv) from rdvprospect)
                and prospections.idprospection = ?";

        $exeq = $this->db->query($req, $idprospection);
        $result = $exeq->result_array();

        return $result;
    }*/
    
    public function getProspectionById($idprospection) {

        $req = "select * 
                from prospections,rdvprospect
                where prospections.idprospection = rdvprospect.idprospection
                and rdvprospect.daterdv = (select max(rdvprospect.daterdv) from rdvprospect where rdvprospect.idprospection = ?)
                and prospections.idprospection = ?";

        $exeq = $this->db->query($req, array($idprospection,$idprospection));
        $result = $exeq->result_array();

        return $result;
    }

}
