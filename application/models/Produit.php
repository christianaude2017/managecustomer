<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Agent
 *
 * @author DJAMARA
 */
class Produit extends CI_Model {
    //put your code here
    
    public function __construct() {
        parent::__construct();
        
    }
    public function getAllAgent(){
            
        $sql = "select * from personne,ville,commune 
                where personne.ville_idville = ville.idville
                and personne.commune_idcommune = commune.idcommune
                and rolePersonne_idrolePersonne = 2 and top_actif = 1 order by Nompersonne";
        $exeq = $this->db->query($sql);
        $result = $exeq->result_array();
        return $result;
            
    }
    
    public function getAllResponsable(){
            
        $sql = "select * from personne,ville,commune 
                where personne.ville_idville = ville.idville
                and personne.commune_idcommune = commune.idcommune
                and rolePersonne_idrolePersonne = 2 and top_actif = 1 and top_respo = 1 order by Nompersonne";
        $exeq = $this->db->query($sql);
        $result = $exeq->result_array();
        return $result;
            
    }
    public function getAllNonResponsable(){
            
        $sql = "select * from personne,ville,commune 
                where personne.ville_idville = ville.idville
                and personne.commune_idcommune = commune.idcommune
                and rolePersonne_idrolePersonne = 2 and top_actif = 1 and top_respo = 0 order by Nompersonne";
        $exeq = $this->db->query($sql);
        $result = $exeq->result_array();
        return $result;
            
    }
    
    public function getAgentByRespo($idRespo){
        
        $sql = "SELECT responsable_agent.Personne_idAgent 
            FROM `responsable_agent` WHERE responsable_agent.Personne_idResponsable = ? ";
        $exeq = $this->db->query($sql, array($idRespo));
        
        $result = $exeq->result();
        return $result;
    }
    
    public function getAgentById($idAgent){
            
        $sql = "select * from personne,ville,commune 
                where personne.ville_idville = ville.idville
                and personne.commune_idcommune = commune.idcommune
                and rolePersonne_idrolePersonne = 2 and idPersonne = ?";
        $exeq = $this->db->query($sql, array($idAgent));
        $result = $exeq->result();
        return $result;
            
    }
    
    public function getCategProdByIdSoc($idSociete){
            
        $sql = "select * from catgorieproduit 
                where societe_idsociete = ?";
        $exeq = $this->db->query($sql, array($idSociete));
        $result = $exeq->result_array();
        return $result;
            
    }
    public function getProduitBySociete($idSociete){
            
        $sql = "select * from produits 
                where societe_idsociete = ? and top_actif = 1";
        $exeq = $this->db->query($sql, array($idSociete));
        $result = $exeq->result_array();
        return $result;
            
    }
    
    public function getPeriodeBySociete($idSociete){
            
        $sql = "select * from periodicitepaie 
                where idSociete = ? and top_actif = 1";
        $exeq = $this->db->query($sql, array($idSociete));
        $result = $exeq->result_array();
        return $result;
            
    }
    
    public function getPrimeBySociete($idSociete){
            
        $sql = "select * from prime,produits 
                where produits.societe_idsociete = ? and produits.top_actif = 1";
        $exeq = $this->db->query($sql, array($idSociete));
        $result = $exeq->result_array();
        return $result;
            
    }
    public function getProduitById($idproduit,$idSociete){
            
        $sql = "select * from produits,	catgorieproduit
                where produits.idcatgorieproduit = catgorieproduit.idcatgorieproduit
                and produits.societe_idsociete = ? and produits.idproduits = ? and top_actif = 1";
        $exeq = $this->db->query($sql, array($idSociete,$idproduit));
        $result = $exeq->result();
        return $result;
            
    }
}
