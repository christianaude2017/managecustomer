<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public $_SESSION = array();
    public $id;

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();

        $this->load->helper('url_helper');
        //$this->load->library('session');
        $this->load->model('Agent');
        $this->load->model('Produit');
        $this->load->model('Prospection');
        $this->load->model('Client');
        $this->load->model('Contrat');

        if (!isset($_SESSION)) {
            $this->load->view('administration/login.php');
        }
    }

    public function setSession() {
        
    }

    public function openSession() {

        $id = $_GET['id'];

        $agent = $this->Agent->getUserById($id);
        $agent = $agent[0];
        if (count($agent) > 0) {
            $tab = array("statut" => session_status(), "data" => $agent);

            $sessionData = array(
                'idUser' => $id/*$agent->idPersonne*/,
                'name' => $agent->NomPersonne . ' ' . $agent->PrenomPersonne,
                'idsociete' => $agent->societe_idsociete,
                'topAdmin' => $agent->top_admin,
                'societeLibelle' => $agent->Libellesociete
            );

            $this->session->set_userdata($sessionData);
            $this->afficherVue('accueil.php', $tab);
        } else {
            show_404();
        }
    }

    public function endSession() {
        session_abort();
        
        //echo "l'etat de la session est :" . session_status();
        $tab = array("statut" => session_status());
        $this->load->view('administration/login.php', $tab);
    }

    public function connexion() {
        $tab = array();
        $this->load->view('administration/login.php', $tab);
    }

    public function register() {
        $tab = array();
        $this->load->view('administration/register.php', $tab);
    }

    public function afficherVue($page = 'accueil.php', $tab = array()) {

        //echo "l'etat de la session est :" . session_status();
        if (session_status() == 2) {
            $this->load->view('administration/entete.php');
            $this->load->view('administration/' . $page, $tab);
            $this->load->view('administration/piedPage.php');
            
        }else{
            $this->endSession();
        }
    }

    public function ajoutrole() {
        $tab = array();
        $this->afficherVue("ajout-role.php", $tab);
    }

    public function listerole() {

        $this->afficherVue("liste-role.php");
    }

    public function ajoutresponsable() {
        $tab = array();
        $agents = $this->Agent->getAllNonResponsable();
        $data = array("agents" => $agents);

        $this->afficherVue("ajout-responsable.php", $data);
    }

    public function listeresponsable() {
        $tab = array();
        $agents = $this->Agent->getAllResponsable();

        $data = array("agents" => $agents);
        $this->afficherVue("liste-responsable.php", $data);
    }

    public function modifierResponsable() {

        $tab = array();
        $Allagents = $this->Agent->getAllNonResponsable();
        $agent = $this->Agent->getAgentById(9);
        $agentsRespo = $this->Agent->getAgentByRespo(9);
        $listofAgent = array();

        foreach ($agentsRespo as $key) {
            array_push($listofAgent, $key->Personne_idAgent);
        }

        $donnees = array("Allagents" => $Allagents, "agentRespo" => $agent[0], "agentofRespo" => $listofAgent);

        $this->afficherVue("modifier-responsable.php", $donnees);
    }

    public function ajoutagent() {
        $tab = array();
        $this->afficherVue("ajout-agent.php", $tab);
    }

    public function modifieragent() {
        $page = "modifier-agent";

        $idAgent = base64_decode($_GET['d']);
        $agent = $this->Agent->getAgentById($idAgent);


        if (count($agent) > 0) {
            $donnes = array("data" => $agent);
            $this->load->view('administration/entete.php');
            $this->load->view('administration/' . $page, $donnes);
            $this->load->view('administration/piedPage.php');
        } else {
            show_404();
        }
    }

    public function deleteAgent() {
        
    }

    public function listeagent() {

        $idsociete = $_SESSION['idsociete'];
        $agents = $this->Agent->getAllAgentbySociete($idsociete);

        $data = array("agents" => $agents);
        $this->afficherVue("liste-agent.php", $data);
    }

    public function ajoutcategorieproduit() {
        $this->afficherVue("ajout-categorie-produit.php");
    }

    public function listecategorieproduit() {
        $this->afficherVue("liste-categorie-produit.php");
    }

    public function modifierproduit() {
        $page = "modifier-produit";

        $idProduit = base64_decode($_GET['d']);
        $produit = $this->Produit->getProduitById($idProduit, $_SESSION['idsociete']);
        $produit = $produit[0];
        $CategProd = $this->Produit->getCategProdByIdSoc($_SESSION['idsociete']);


        if (count($produit) > 0) {
            $donnes = array("data" => $produit, "listCateg" => $CategProd);
            $this->load->view('administration/entete.php');
            $this->load->view('administration/' . $page, $donnes);
            $this->load->view('administration/piedPage.php');
        } else {
            show_404();
        }
    }

    public function ajoutproduit() {

        $CategProd = $this->Produit->getCategProdByIdSoc($_SESSION['idsociete']);
        $listeCategProd = array();
        foreach ($CategProd as $key) {
            array_push($listeCategProd, $key);
        }
        $data = array("listCateg" => $listeCategProd);

        $this->afficherVue("ajout-produit.php", $data);
    }

    public function listeproduit() {

        $Produits = $this->Produit->getProduitBySociete($_SESSION['idsociete']);
        $data = array("produits" => $Produits);
        $this->afficherVue("liste-produit.php", $data);
    }

    public function ajoutprime() {
        
        $Produits = $this->Produit->getProduitBySociete($_SESSION['idsociete']);
        $data = array("produits" => $Produits);
        $this->afficherVue("ajout-prime.php",$data);
    }

    public function listeprime() {
        
        
        $Prime = $this->Produit->getPrimeBySociete($_SESSION['idsociete']);
        $data = array("primes" => $Prime);
        $this->afficherVue("liste-prime.php",$data);
    }

    public function ajoutprospection() {

        $idsociete = $_SESSION['idsociete'];
        $agents = $this->Agent->getAllAgentbySociete($idsociete);

        $Produits = $this->Produit->getProduitBySociete($idsociete);

        $data = array("agents" => $agents, "produits" => $Produits);
        $this->afficherVue("ajout-prospection.php", $data);
    }

    public function modifierprospection() {

        $idProspection = base64_decode($_GET['d']);
        $prospections = $this->Prospection->getProspectionById($idProspection);
        $prospections = $prospections[0];

        $idsociete = $_SESSION['idsociete'];
        $agents = $this->Agent->getAllAgentbySociete($idsociete);
        $Produits = $this->Produit->getProduitBySociete($idsociete);

        $data = array("agents" => $agents, "produits" => $Produits, "prospection" => $prospections);
        $this->afficherVue("modifier-prospection.php", $data);
    }
    public function modifierRendezVous() {

        $idProspection = base64_decode($_GET['d']);
        $idsociete = $_SESSION['idsociete'];
        $prospections = $this->Prospection->getProspectionById($idProspection);
        $prospections = $prospections[0];

        
        $agents = $this->Agent->getAllAgentbySociete($idsociete);
        $Produits = $this->Produit->getProduitBySociete($idsociete);

        $data = array("agents" => $agents, "produits" => $Produits, "prospection" => $prospections);
        $this->afficherVue("modifier-rdv.php", $data);
    }

    public function listeprospection() {

        $idsociete = $_SESSION['idsociete'];
        $idagent = $_SESSION['idUser'];
        $topAdmin = $_SESSION['topAdmin'];
        
        $prospections = "";
        
        if($topAdmin == 0){
            $prospections = $this->Prospection->getAllprospectionBysoc($idsociete,$idagent);
        }else{
            $prospections = $this->Prospection->getAllprospection($idsociete);
        }
        

        $data = array("prospections" => $prospections);
        $this->afficherVue("liste-prospection.php", $data);
    }
    
    public function listeRendezVous() {

        $idsociete = $_SESSION['idsociete'];
        $idagent = $_SESSION['idUser'];
        $topAdmin = $_SESSION['topAdmin'];
        
        $prospections = "";
        
        if($topAdmin == 0){
            //$prospections = $this->Prospection->getAllprospectionBysoc($idsociete,$idagent);
            $prospections = $this->Prospection->getAllRendezVousBysoc($idsociete,$idagent);
        }else{
            $prospections = $this->Prospection->getAllRendezVous($idsociete);
        }
        

        $data = array("prospections" => $prospections);
        $this->afficherVue("liste-rendezvous.php", $data);
    }


    public function ajoutclient() {

        $idsociete = $_SESSION['idsociete'];
        $idagent = $_SESSION['idUser'];
        $prospections = $this->Prospection->getAllprospectionBysoc($idsociete,$idagent);

        $data = array("prospections" => $prospections);
        $this->afficherVue("ajout-client.php", $data);
    }
    
    public function modifierClient() {

        $idClient = base64_decode($_GET['d']);
        $idsociete = $_SESSION['idsociete'];
        $idagent = $_SESSION['idUser'];
        
        $client = $this->Client->getClientById($idClient);
        $client = $client[0];
        
        $prospections = $this->Prospection->getAllprospectionBysoc($idsociete,$idagent);

        $data = array("prospections" => $prospections,"client"=>$client);
        $this->afficherVue("modifier-client.php", $data);
    }

    public function listeclient() {
        
        $idsociete = $_SESSION['idsociete'];
        $idagent = $_SESSION['idUser'];
        $topAdmin = $_SESSION['topAdmin'];
        
        $clients = "";
        
        if ($topAdmin == 1) {
            $clients = $this->Client->getAllClientBySoc($idsociete);
        }else{
            $clients = $this->Client->getAllClientByAgent($idagent,$idsociete);
        }
        
        $data = array("clients"=>$clients);
        $this->afficherVue("liste-client.php",$data);
    }

    public function ajoutcontrat() {
        
        $idsociete = $_SESSION['idsociete'];
        $idagent = $_SESSION['idUser'];
        $topAdmin = $_SESSION['topAdmin'];
        
        $nom = $_SESSION['name'];
        $clients = $this->Client->getAllClientByAgent($idagent,$idsociete);
        
        $Produits = $this->Produit->getProduitBySociete($_SESSION['idsociete']);
        
        $Periode = $this->Produit->getPeriodeBySociete($_SESSION['idsociete']);
        
        $modePaie = $this->Contrat->getModePaiementBySociete($_SESSION['idsociete']);
        
        $data = array("nomAgent"=>$nom,"idAagent"=>$idagent,"clients"=>$clients,
            "produits"=>$Produits,"periodes"=>$Periode,"modePaies"=>$modePaie);
        
        $this->afficherVue("ajout-contrat.php",$data);
    }
    
    public function modifierContrat() {
        
        $idContrat = base64_decode($_GET['d']);
        $idsociete = $_SESSION['idsociete'];
        $idagent = $_SESSION['idUser'];
        $topAdmin = $_SESSION['topAdmin'];
        
        $nom = $_SESSION['name'];
        $clients = $this->Client->getAllClientByAgent($idagent,$idsociete);
        
        $Produits = $this->Produit->getProduitBySociete($_SESSION['idsociete']);
        
        $Periode = $this->Produit->getPeriodeBySociete($_SESSION['idsociete']);
        
        $modePaie = $this->Contrat->getModePaiementBySociete($_SESSION['idsociete']);
        
        $contrat = $this->Contrat->getAllContratbyIdContrat($idContrat);
        
        
        
        $data = array("nomAgent"=>$nom,"idAagent"=>$idagent,"clients"=>$clients,
            "produits"=>$Produits,"periodes"=>$Periode,"modePaies"=>$modePaie,"contrat"=>$contrat);
        
        $this->afficherVue("modifier-contrat.php",$data);
    }

    public function listecontrat() {
        
        $idsociete = $_SESSION['idsociete'];
        $idagent = $_SESSION['idUser'];
        $topAdmin = $_SESSION['topAdmin'];
        
        $contrats = "";
        
        if ($topAdmin == 1) {
            $contrats = $this->Contrat->getAllContratbySociete($idsociete);
        }else{
            $contrats = $this->Contrat->getAllContratbySocieteByAgent($idsociete,$idagent);
        }
        
        $data = array("contrats"=>$contrats);
        
        $this->afficherVue("liste-contrat.php",$data);
    }

    public function ajoutmodepaiement() {
        $this->afficherVue("ajout-mode-paiement.php");
    }

    public function listemodepaiement() {
        $this->afficherVue("liste-mode-paiement.php");
    }

    public function ajoutpaiement() {
        $this->afficherVue("ajout-paiement.php");
    }

    public function listepaiement() {
        $this->afficherVue("liste-paiement.php");
    }

    public function parametre() {
        $this->load->view("administration/parametre.php");
    }

    public function statistique() {
        
        $idsociete = $_SESSION['idsociete'];
        $agents = $this->Agent->getAllAgentbySociete($idsociete);
        
        $data = array("agents" =>$agents);
        $this->afficherVue("statistique-agent.php",$data);
    }
    
    public function tableauRecap() {
        
        $idsociete = $_SESSION['idsociete'];
        $agents = $this->Agent->getAllAgentbySociete($idsociete);
        
        $data = array("agents" =>$agents);
        $this->afficherVue("recap-agent.php",$data);
    }

    public function statistique_entreprise() {
        $this->afficherVue("statistique.php");
    }

    /*     * ********************************************************* */

    public function getProspectionById() {

        $idprospect = $_POST['idprospection'];

        $prospections = $this->Prospection->getProspectionById($idprospect);

        echo json_encode($prospections);
    }

}
