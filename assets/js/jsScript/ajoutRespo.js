/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    setRespo();

});

function setRespo() {

    $('#form-ajout-responsable').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            titreresponsabilite: {
                message: 'Remplissez un titre correcte',
                validators: {
                    notEmpty: {
                        message: 'Vous devez remplir un titre pour la responsabilité'
                    },
                    stringLength: {
                        min: 3,
                        max: 1000,
                        message: 'la longueur du nom doit être spérieure à 3 caractères'
                    }
                }
            },
            nomresponsable: {
                validators: {
                    notEmpty: {
                        message: 'Vous devez saisir un nom de responsable'
                    },
                    stringLength: {
                        min: 3,
                        max: 100,
                        message: 'la longueur du prenom doit être spérieure à 3 caractères'
                    }
                }
            },
            listagent: {
                notEmpty: {
                    message: 'Vous devez renseignez les agents svp'
                }
            }
            
        }
    }).on('success.form.bv', function (e) {
        // $(e.target) --> The form instance
        // $(e.target).data('bootstrapValidator')
        //             --> The BootstrapValidator instance
        //             
        //Empecher la soumission par defaut du formulaire
        e.preventDefault();

        $form = $(e.target);
        $form.data('bootstrapValidator').resetForm();

        var bf = $form.data('bootstrapValidator');
        //alert($form.serialize());
        //var data = $form.serialize();
        
        //console.log(data);

        // Do something ...
        insertRespo();

    });
}

function getAgentById(idpersonne) {

    $.ajax({
        url: "http://localhost/apimanagecustomer/public/getagentbyid", //Page de traitement
        type: 'POST',
        data: {
            id: idpersonne
        },
        dataType: 'json',
        //contentType: false,
        //processData: false,
        //cache: false,
        success: function (data, textStatus, jqXHR) {

            console.log(data);
            var data = JSON.stringify(data);

            //console.log(jqXHR);
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-bottom-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "onHidden": function () {
                    $url = "http://localhost/managecustomer/index.php/welcome/modifieragent/?d=" + data;
                    window.location.href = $url;
                }
            }
            toastr.success('INFO', 'Nouvel agent ajoutée');



        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr.warning('ALERTE', "Il se pourrait qu'il ait un problème");
        }
    });
}

function modifierRespo() {

    // Do something ...
    $.ajax({
        url: $form.attr('action'), //Page de traitement
        type: 'POST',
        data: $form.serialize(),
        dataType: 'json',
        //contentType: false,
        //processData: false,
        //cache: false,
        success: function (data, textStatus, jqXHR) {
            console.log(data);

            console.log(jqXHR);
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "onHidden": function () {
                    $url = "http://localhost/managecustomer/index.php/welcome/listeagent";
                    window.location.href = $url;
                }
            }
            toastr.success('Modification effectuée', 'SUCCES');



        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr.warning('ALERTE', "Il se pourrait qu'il ait un problème");
        }
    });

}

function insertRespo() {

    $.confirm({
        title: 'Confirm!',
        content: 'Ajout Nouvel Agent',
        buttons: {
            confirm: {
                btnClass: 'btn-danger',
                action: function () {
                    $.ajax({
                        url: $form.attr('action'), //Page de traitement
                        type: 'POST',
                        data: $form.serialize(),
                        dataType: 'json',
                        //contentType: false,
                        //processData: false,
                        //cache: false,
                        success: function (data, textStatus, jqXHR) {
                            console.log(data);

                            console.log(jqXHR);
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "progressBar": true,
                                "preventDuplicates": false,
                                "positionClass": "toast-top-right",
                                "onclick": null,
                                "showDuration": "400",
                                "hideDuration": "200",
                                "timeOut": "700",
                                "extendedTimeOut": "100",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut",
                                "onHidden": function () {
                                    $url = "http://localhost/managecustomer/index.php/welcome/listeresponsable";
                                    window.location.href = $url;
                                }
                            }
                            toastr.success('nouvel agent crée avec succès', 'SUCCES');
                            


                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(errorThrown);

                          /*  toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "progressBar": true,
                                "preventDuplicates": false,
                                "positionClass": "toast-top-right",
                                "onclick": null,
                                "showDuration": "400",
                                "hideDuration": "1000",
                                "timeOut": "7000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.warning('ALERTE', "Il se pourrait qu'il ait un problème");*/
                        }
                    });
                }
            },
            cancel: function () {
                $.alert('Canceled!');
            }
        }
    });

}

function deleteAgent(idagent) {

    $.confirm({
        title: 'Confirm!',
        content: 'Simple confirm!',
        draggable: true,
        buttons: {

            confirm: {
                btnClass: 'btn-green',
                action: function () {
                    $.ajax({
                        url: "http://localhost/apimanagecustomer/public/delagentbyid", //Page de traitement
                        type: 'POST',
                        data: {id: idagent},
                        dataType: 'json',
                        //contentType: false,
                        //processData: false,
                        //cache: false,
                        success: function (data, textStatus, jqXHR) {
                            console.log(data);

                            console.log(jqXHR);
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "progressBar": true,
                                "preventDuplicates": false,
                                "positionClass": "toast-top-right",
                                "onclick": null,
                                "showDuration": "400",
                                "hideDuration": "200",
                                "timeOut": "700",
                                "extendedTimeOut": "100",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut",
                                "onHidden": function () {
                                    $url = "http://localhost/managecustomer/index.php/welcome/listeagent";
                                    window.location.href = $url;
                                }
                            }
                            toastr.success('nouvel agent crée avec succès', 'SUCCES');



                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(errorThrown);

                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "progressBar": true,
                                "preventDuplicates": false,
                                "positionClass": "toast-top-right",
                                "onclick": null,
                                "showDuration": "400",
                                "hideDuration": "1000",
                                "timeOut": "7000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.warning('ALERTE', "Il se pourrait qu'il ait un problème");
                        }
                    });
                }
            },
            cancel: function () {
                $.alert('Canceled!');
            }/*,
             somethingElse: {
             text: 'Something else',
             btnClass: 'btn-blue',
             keys: ['enter', 'shift'],
             action: function () {
             $.alert('Something else?');
             }
             }*/
        }
    });
    /**/
}