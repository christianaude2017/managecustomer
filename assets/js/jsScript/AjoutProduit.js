/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    setProduit();

});

function setProduit() {

    $('#form-add-produit').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            libelleproduit: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Vous devez saisir un nom pour votre produit'
                    },
                    stringLength: {
                        min: 3,
                        max: 30,
                        message: 'la longueur du nom doit être spérieure à 3 caractères'
                    }
                }
            },
            montantproduit: {
                validators: {
                    notEmpty: {
                        message: 'Vous devez saisir le montant de votre produit'
                    },
                    /*digits: {
                        message: 'le montant doit contenir seulement que des chiffres'
                    },*/
                    regexp: {
                        regexp: /^[0-9\,]+$/i,
                        message: 'le montant doit comporter uniquement que des chiffres'
                    }
                }
            },
            commission: {
                validators: {
                    notEmpty: {
                        message: 'Vous devez saisir le pourcentage de la commission sur le votre produit'
                    },
                    /*digits: {
                        message: 'le montant doit contenir seulement que des chiffres'
                    },*/
                    between: {
                        min: 0,
                        max: 100,
                        message: 'le pourcentage doit être compris entre 0% et 100%'
                    }
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // $(e.target) --> The form instance
        // $(e.target).data('bootstrapValidator')
        //             --> The BootstrapValidator instance
        //             
        //Empecher la soumission par defaut du formulaire
        e.preventDefault();

        $form = $(e.target);
        $form.data('bootstrapValidator').resetForm();

        var bf = $form.data('bootstrapValidator');
        console.log($form.serialize());
        //console.log($form.attr('action'));

        // Do something ...
        insertProduit();
        
        //alert($form.serialize());


    });
}

function modifierProduit() {

    // Do something ...
    $.ajax({
        url: $form.attr('action'), //Page de traitement
        type: 'POST',
        data: $form.serialize(),
        dataType: 'json',
        //contentType: false,
        //processData: false,
        //cache: false,
        success: function (data, textStatus, jqXHR) {
            console.log(data);

            console.log(jqXHR);
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "onHidden": function () {
                    $url = "http://localhost/managecustomer/index.php/welcome/listeagent";
                    window.location.href = $url;
                }
            }
            toastr.success('Modification effectuée', 'SUCCES');



        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr.warning('ALERTE', "Il se pourrait qu'il ait un problème");
        }
    });

}

function insertProduit() {

    $.confirm({
        title: 'Confirm!',
        content: 'Simple confirm!',
        draggable: true,
        buttons: {

            confirm: {
                btnClass: 'btn-green',
                action: function () {
                    $.ajax({
                        url: $form.attr('action'), //Page de traitement
                        type: 'POST',
                        data: $form.serialize(),
                        dataType: 'json',
                        //contentType: false,
                        //processData: false,
                        //cache: false,
                        success: function (data, textStatus, jqXHR) {
                            console.log(data);
                            if (data["message"] === "aucune correspondance") {

                                console.log(jqXHR);
                                toastr.options = {
                                    "closeButton": true,
                                    "debug": false,
                                    "progressBar": true,
                                    "preventDuplicates": false,
                                    "positionClass": "toast-top-right",
                                    "onclick": null,
                                    "showDuration": "100",
                                    "hideDuration": "50",
                                    "timeOut": "7000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut",
                                    /*"onHidden": function () {
                                     $url = "http://localhost/managecustomer/index.php/welcome/openSession";
                                     window.location.href = $url;
                                     }*/
                                }
                                toastr.warning('Veuillez verifier vos identifiants', 'ALERTE');
                            } else {
                                console.log(jqXHR);
                                toastr.options = {
                                    "closeButton": true,
                                    "debug": false,
                                    "progressBar": true,
                                    "preventDuplicates": false,
                                    "positionClass": "toast-top-right",
                                    "onclick": null,
                                    "showDuration": "400",
                                    "hideDuration": "1000",
                                    "timeOut": "7000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut",
                                    "onHidden": function () {
                                        $url = "http://localhost/managecustomer/index.php/welcome/listeproduit";
                                        window.location.href = $url;
                                    }
                                }
                                toastr.success('Enregistrement du produit réussi', 'SUCCES');
                            }

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(errorThrown);
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "progressBar": true,
                                "preventDuplicates": false,
                                "positionClass": "toast-top-right",
                                "onclick": null,
                                "showDuration": "400",
                                "hideDuration": "1000",
                                "timeOut": "7000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.warning('ALERTE', "Il se pourrait qu'il ait un problème");
                        }
                    });
                }
            },
            cancel: function () {
                $.alert('Canceled!');
            }/*,
             somethingElse: {
             text: 'Something else',
             btnClass: 'btn-blue',
             keys: ['enter', 'shift'],
             action: function () {
             $.alert('Something else?');
             }
             }*/
        }
    });
}

function addCategorie() {

    var categorie = $('#categorie').val();
    var societe = $('#idsociete').val();
    /*alert(societe);*/

    /*$.alert({
     title: 'Confirm!',
     content: 'Ajout Nouvel Agent',
     })*/
    $.ajax({
        url: "http://localhost/apimanagecustomer/public/addCategorie", //Page de traitement
        type: 'POST',
        data: {
            categorieLibelle: categorie,
            idsociete: societe
        },
        dataType: 'json',
        //contentType: false,
        //processData: false,
        //cache: false,
        success: function (data, textStatus, jqXHR) {
            console.log(data);
            $url = "http://localhost/managecustomer/index.php/welcome/ajoutproduit";
            window.location.href = $url;
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    })
}

function setactive(idProduit, etatActu) {

    var categorie = $('#categorie').val();
    var societe = $('#idsociete').val();
    var etat = 0;
    if (etatActu == 1) {
        etat = 0;
    } else {
        etat = 1;
    }
    /*alert(societe);*/

    /*$.alert({
     title: 'Confirm!',
     content: 'Ajout Nouvel Agent',
     })*/
    $.ajax({
        url: "http://localhost/apimanagecustomer/public/activeProduit", //Page de traitement
        type: 'POST',
        data: {
            produit: idProduit,
            etat: etat
        },
        dataType: 'json',
        //contentType: false,
        //processData: false,
        //cache: false,
        success: function (data, textStatus, jqXHR) {
            console.log(data);
            $url = "http://localhost/managecustomer/index.php/welcome/ajoutproduit";
            /*window.location.href = $url;*/

        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    })
}

