/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    c3.generate({
        bindto: '#gauge',
        data: {
            columns: [
                ['data', 11.4]
            ],

            type: 'gauge'
        },
        color: {
            pattern: ['#c2265d', '#BABABA']

        }
    });



    var NomAgent = $("#idAgent option:selected").text();

    getProspectionsByDay($("#idAgent").val(), function (output) {
        graphBar(output[0], output[1], output[2], NomAgent);
    });

    $('#Total,#totalMontant').css("background-color", "#19b394");
    $('#Total,#totalMontant').css("color", "white");
    $('#Total,#totalMontant').css("font-weight", "bold");
    $('#totalMontant').addClass("money");
    

    getProspectionsByTime($("#idAgent").val(), 1); // 1 est une valeur quelconque, aucune influence sur le système
    getContratByTime($("#idAgent").val(), 1);

    /*getProspectionsByTime($("#idAgent").val(), function (output) {
     alert(JSON.stringify(output));
     
     
     if (output.length > 0) {
     $.each(output, function (key, output) {
     $('#tbodyprop').append(
     '<tr>'
     + '<td>' + output.idprospection + '</td>'
     + '<td>' + output.dateprospection + '</td>'
     + '<td>' + output.nomprospection + ' ' + output.prenomprospection + '</td>'
     + '</tr>'
     );
     $('#Total').html('Total prospect: ' + output.nombreprospTotalAgent);
     
     });
     }
     $("#table-liste-prospection").DataTable({
     pageLength: 5,
     responsive: true,
     retrieve: true,
     destroy: true
     });
     
     
     });*/

    $("#idAgent").change(function () {
        /*getProspectionsByTime($("#idAgent").val(), function (output) {
         //alert(JSON.stringify(output));
         
         //table.destroy();
         $('#tbodyprop').html("");
         if (output.length > 0) {
         $.each(output, function (key, output) {
         $('#tbodyprop').append(
         '<tr>'
         + '<td>' + output.idprospection + '</td>'
         + '<td>' + output.dateprospection + '</td>'
         + '<td>' + output.nomprospection + ' ' + output.prenomprospection + '</td>'
         + '</tr>'
         );
         
         $('#Total').html('Total prospect: ' + output.nombreprospTotalAgent);
         });
         }
         
         $("#table-liste-prospection").DataTable({
         pageLength: 5,
         responsive: true
         });
         
         });*/
        getProspectionsByTime($("#idAgent").val(), 1);
        getContratByTime($("#idAgent").val(), 1);
    });

    $("#search").click(function () {
        getProspectionsByTime($("#idAgent").val(), 1);
        getContratByTime($("#idAgent").val(), 1);
    });



    $("#idAgent").change(function () {

        var NomAgent = $("#idAgent option:selected").text();
        var IdAgent = $("#idAgent option:selected").val();

        getProspectionsByDay(IdAgent, function (output) {
            graphBar(output[0], output[1], output[2], NomAgent);
        });
    });

    $("#search").click(function () {

        var NomAgent = $("#idAgent option:selected").text();
        var IdAgent = $("#idAgent option:selected").val();

        getProspectionsByDay(IdAgent, function (output) {
            graphBar(output[0], output[1], output[2], NomAgent);
        });
    });


    //graphBar();
});


function graphBar(x, y, z, nomAgent) {
    var lineData = {
        labels: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre",
            "Octobre", "Novembre", "Décembre"],
        datasets: [

            {
                label: "Prospects recensés",
                backgroundColor: 'rgba(26,179,148,0.5)',
                borderColor: "rgba(26,179,148,0.7)",
                pointBackgroundColor: "rgba(26,179,148,1)",
                pointBorderColor: "#fff",
                data: [28, 48, 40, 19, 86, 27, 90, 14, 74, 52, 78, 15]
            },
            {
                label: "Prospects devenus clients",
                backgroundColor: '#c2265d',
                pointBorderColor: "#fff",
                data: [65, 59, 80, 81, 56, 55, 40, 48, 56, 74, 85, 20]
            }
        ]
    };

    var lineOptions = {
        responsive: true
    };


    var ctx = document.getElementById("lineChart").getContext("2d");
    new Chart(ctx, {type: 'line', data: lineData, options: lineOptions});

    var barData = {
        labels: [],
        datasets: [
            {
                label: "nombre de prospections",
                backgroundColor: 'rgba(220, 220, 220, 0.5)',
                pointBorderColor: "#fff",
                data: [x, 10]
            },
            {
                label: "nombre de Contrats",
                backgroundColor: '#c2265d',
                pointBorderColor: "#c2265d",
                pointBackgroundColor: "#c2265d",

                data: [y]
            },
            {
                label: "Prospects devenus clients",
                backgroundColor: 'rgba(26,179,148,0.5)',
                borderColor: "rgba(26,179,148,0.7)",
                pointBackgroundColor: "rgba(26,179,148,1)",
                pointBorderColor: "#fff",
                data: [z]
            }

        ]
    };

    var barOptions = {
        responsive: true,
        title: {
            display: true,
            text: "Graphe statistique - Nombre clients et des prospects obtenus pour " + nomAgent
        },
        scales: {
            yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
        }
    };


    var ctx2 = document.getElementById("barChart").getContext("2d");
    var chart = new Chart(ctx2, {type: 'bar', data: barData, options: barOptions});

}
;

$(function () {
    var doughnutData = {
        labels: ["Revenus venant de prospects", "Revenus venant de cliants directs"],
        datasets: [{
                data: [350000, 150000],
                backgroundColor: ["#a3e1d4", "#b5b8cf"]
            }]
    };


    var doughnutOptions = {
        responsive: true
    };


    var ctx4 = document.getElementById("doughnutChart").getContext("2d");
    new Chart(ctx4, {type: 'doughnut', data: doughnutData, options: doughnutOptions});
});

$(function () {
    Morris.Line({
        element: 'morris-line-chart',
        data: [{y: '2006', a: 100, b: 90},
            {y: '2007', a: 75, b: 65},
            {y: '2008', a: 50, b: 40},
            {y: '2009', a: 75, b: 65},
            {y: '2010', a: 50, b: 40},
            {y: '2011', a: 75, b: 65},
            {y: '2012', a: 100, b: 90}],
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Series A', 'Series B'],
        hideHover: 'auto',
        resize: true,
        lineColors: ['#54cdb4', '#1ab394'],
    });

    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{label: "Download Sales", value: 12},
            {label: "In-Store Sales", value: 30},
            {label: "Mail-Order Sales", value: 20}],
        resize: true,
        colors: ['#87d6c6', '#54cdb4', '#1ab394'],
    });
});

function getProspectionsByDay(idAgent, handleData) {

    var datedebut;
    var datefin;

    if ($("#datedebut").val() == "") {
        datedebut = "1";
    } else {
        datedebut = 'dateprospection >' + "'" + $("#datedebut").val() + "'";
    }
    if ($("#datefin").val() == "") {
        datefin = "1";
    } else {
        datefin = 'dateprospection < ' + "'" + $("#datefin").val() + "'";
    }


    $.ajax({
        url: "http://localhost/apimanagecustomer/public/getStatByAgent", //Page de traitement
        type: 'POST',
        data: {idAgent: idAgent},
        dataType: 'json',
        //contentType: false,
        //processData: false,
        //cache: false,
        success: function (data, textStatus, jqXHR) {
            console.log(data);
            handleData(data);
            //console.log(jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);

        }
    });

}


function getProspectionsByTime(idAgent, handleData) {

    var datedebut = $("#datedebut").val();
    var datefin = $("#datefin").val();

    $.ajax({
        url: "http://localhost/apimanagecustomer/public/getProspByAgent", //Page de traitement
        type: 'POST',
        data: {idAgent: idAgent,
            datedebut: datedebut,
            datefin: datefin
        },
        dataType: 'json',
        //contentType: false,
        //processData: false,
        //cache: false,
        success: function (data, textStatus, jqXHR) {
            console.log(data);
            var output = data;
            
            if ($.fn.DataTable.isDataTable("#table-liste-prospection")) {

                $("#table-liste-prospection").DataTable().destroy();

                $('#tbodyprop').html("");

                if (output.length > 0) {
                    $.each(output, function (key, output) {
                        $('#tbodyprop').append(
                                '<tr>'
                                + '<td>' + output.idprospection + '</td>'
                                + '<td>' + output.dateprospection + '</td>'
                                + '<td>' + output.nomprospection + ' ' + output.prenomprospection + '</td>'
                                + '</tr>'
                                );
                        $('#Total').html('Total prospect: ' + output.nombreprospTotalAgent);

                    });

                }
                $("#table-liste-prospection").DataTable({
                    pageLength: 5,
                    responsive: true,
                });

                //$("#table-liste-prospection").DataTable();
            } else {

                $('#tbodyprop').html("");

                if (output.length > 0) {
                    $.each(output, function (key, output) {
                        $('#tbodyprop').append(
                                '<tr>'
                                + '<td>' + output.idprospection + '</td>'
                                + '<td>' + output.dateprospection + '</td>'
                                + '<td>' + output.nomprospection + ' ' + output.prenomprospection + '</td>'
                                + '</tr>'
                                );
                        $('#Total').html('Total prospect: ' + output.nombreprospTotalAgent);

                    });

                }
                $("#table-liste-prospection").DataTable({
                    pageLength: 5,
                    responsive: true,
                });
            }

            //console.log(jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);

        }
    });

}


function getContratByTime(idAgent, handleData) {

    var datedebut = $("#datedebut").val();
    var datefin = $("#datefin").val();

    $.ajax({
        url: "http://localhost/apimanagecustomer/public/getContratByAgent", //Page de traitement
        type: 'POST',
        data: {idAgent: idAgent,
            datedebut: datedebut,
            datefin: datefin
        },
        dataType: 'json',
        //contentType: false,
        //processData: false,
        //cache: false,
        success: function (data, textStatus, jqXHR) {
            console.log(data);
            var output = data;

            if ($.fn.DataTable.isDataTable("#table-liste-contrat")) {

                $("#table-liste-contrat").DataTable().destroy();

                $('#tbodyContrat').html("");

                if (output.length > 0) {
                    $.each(output, function (key, output) {
                        $('#tbodyContrat').append(
                                '<tr>'
                                + '<td>' + output.idcontrat + '</td>'
                                + '<td>' + output.dateContrat + '</td>'
                                + '<td>' + output.Libelleproduits + '</td>'
                                + '<td>' + output.dateContrat + '</td>'
                                + '<td>' + output.NomClient + ' ' + output.PrenomClient + '</td>'
                                + '</tr>'
                                );
                        $('#totalContrat').html('Total contrat: ' + output.nombrecontrat);
                        $('#totalMontant').html('Total montant: ' + output.Totalmontantcontrat);

                    });

                }
                $("#table-liste-contrat").DataTable({
                    pageLength: 5,
                    responsive: true,
                });

                //$("#table-liste-prospection").DataTable();
            } else {

                $('#tbodyContrat').html("");

                if (output.length > 0) {
                    $.each(output, function (key, output) {
                        $('#tbodyContrat').append(
                                '<tr>'
                                + '<td>' + output.idcontrat + '</td>'
                                + '<td>' + output.dateContrat + '</td>'
                                + '<td>' + output.NomClient + ' ' + output.PrenomClient + '</td>'
                                + '<td class="money">'+output.Libelleproduits+'</td>'
                                + '<td>'+output.montantContrat+'</td>'
                                + '</tr>'
                                );
                        $('#totalContrat').html('Total contrat: ' + output.nombrecontrat);
                        $('#totalMontant').html('Total montant: ' + output.Totalmontantcontrat);

                    });

                }
                $("#table-liste-contrat").DataTable({
                    pageLength: 7,
                    responsive: true
                });
            }

            //console.log(jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);

        }
    });

}