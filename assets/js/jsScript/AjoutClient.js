/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



$(document).ready(function () {

    choiceprospect();
    //alert("Bonjour je suis dans le formulaire avec js AjoutClient");
    $('#formSelectProspect').hide();

    $('#choixAction').change(function () {
        if ($(this).val() == 1) {
            $('#formNewClient').show('500');
            $('#formSelectProspect').hide('500');
        } else {
            $('#formSelectProspect').show('500');
            $('#formNewClient').hide('500');
        }
    });


    setClient();
    setClientProsp();

});

function choiceprospect() {

    var idprospection = $("#idprospect").val();

    //alert(idprospection);
    $.ajax({
        url: "http://localhost/managecustomer/index.php/Welcome/getProspectionById", //Page de traitement
        type: 'POST',
        data: {
            idprospection: idprospection
        },
        dataType: 'json',
        //contentType: false,
        //processData: false,
        //cache: false,
        success: function (data, textStatus, jqXHR) {
            console.log(JSON.stringify(data));
            data = data[0];
            //alert(data.agentprospection);

            $("#societeprospect").val(data.societeprospection);
            $("#fonctionprospect").val(data.fonctionprospection);
            $("#telephoneprospect").val(data.telephoneprospection);
            $("#emailprospect").val(data.emailprospection);
            $("#idnomprospect").val(data.nomprospection);
            $("#idprenomprospect").val(data.prenomprospection);
        }

    });
}
/*function choixTypeEntre() {
 
 if ($('#choixAction').val() === 1) {
 $('#formNewClient').show('500');
 $('#formSelectProspect').hide('500');
 idform = "#formNewClient";
 }
 if($('#choixAction').val() === 2) {
 $('#formSelectProspect').show('500');
 $('#formNewClient').hide('500');
 idform = "#formSelectProspect";
 }
 }*/

function setClient() {

    $("#formNewClient").bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nomclient: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Vous devez saisir un nom pour votre produit'
                    },
                    stringLength: {
                        min: 3,
                        max: 30,
                        message: 'la longueur du nom doit être spérieure à 3 caractères'
                    }
                }
            },
            prenomclient: {
                validators: {
                    notEmpty: {
                        message: 'Vous devez saisir le montant de votre produit'
                    },
                    stringLength: {
                        min: 3,
                        max: 30,
                        message: 'la longueur du nom doit être spérieure à 3 caractères'
                    }
                    /*digits: {
                     message: 'le montant doit contenir seulement que des chiffres'
                     },*/
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // $(e.target) --> The form instance
        // $(e.target).data('bootstrapValidator')
        //             --> The BootstrapValidator instance
        //             
        //Empecher la soumission par defaut du formulaire
        e.preventDefault();
        $form = $(e.target);
        $form.data('bootstrapValidator').resetForm();
        var bf = $form.data('bootstrapValidator');
        //console.log($form.serialize());

        //alert($form.serialize());

        insertClient();
        //RecupererTouslesFile(23);
    });

}

function setClientProsp() {


    $("#formSelectProspect").bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {

            nomprospect: {
                validators: {
                    notEmpty: {
                        message: 'Vous devez saisir le montant de votre produit'
                    },
                    stringLength: {
                        min: 3,
                        max: 30,
                        message: 'la longueur du nom doit être spérieure à 3 caractères'
                    }
                    /*digits: {
                     message: 'le montant doit contenir seulement que des chiffres'
                     },*/
                }
            },
            emailprospect: {
                validators: {
                    notEmpty: {
                        message: 'Vous devez saisir le montant de votre produit'
                    }
                    /*digits: {
                     message: 'le montant doit contenir seulement que des chiffres'
                     },*/
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // $(e.target) --> The form instance
        // $(e.target).data('bootstrapValidator')
        //             --> The BootstrapValidator instance
        //             
        //Empecher la soumission par defaut du formulaire
        e.preventDefault();
        $form = $(e.target);
        $form.data('bootstrapValidator').resetForm();
        var bf = $form.data('bootstrapValidator');
        //console.log($form.serialize());

        //alert($form.serialize());

        insertClient();
    });
}

function insertClient() {

    $.confirm({
        title: 'Confirm!',
        content: 'Ajout Nouveau Client',
        buttons: {
            confirm: {
                btnClass: 'btn-danger',
                action: function () {
                    $.ajax({
                        url: $form.attr('action'), //Page de traitement
                        type: 'POST',
                        data: $form.serialize(),
                        dataType: 'json',
                        //contentType: false,
                        //processData: false,
                        //cache: false,
                        success: function (data, textStatus, jqXHR) {
                            console.log(data);

                            console.log(jqXHR);
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "progressBar": true,
                                "preventDuplicates": false,
                                "positionClass": "toast-top-right",
                                "onclick": null,
                                "showDuration": "400",
                                "hideDuration": "1000",
                                "timeOut": "7000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut",
                                "onHidden": function () {
                                    $.confirm({
                                        title: 'Confirm!',
                                        content: 'Voulez-vous enregistrer un contrat ?',
                                        draggable: true,
                                        buttons: {
                                            confirm: {
                                                btnClass: 'btn-green',
                                                action: function () {
                                                    $url = "http://localhost/managecustomer/index.php/welcome/ajoutcontrat";
                                                    window.location.href = $url;
                                                }
                                            }, cancel: function () {
                                                $url = "http://localhost/managecustomer/index.php/welcome/listeclient";
                                                window.location.href = $url;
                                            }
                                        },
                                    });
                                }
                            }
                            toastr.success('nouvel agent crée avec succès', 'SUCCES');



                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(errorThrown);

                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "progressBar": true,
                                "preventDuplicates": false,
                                "positionClass": "toast-top-right",
                                "onclick": null,
                                "showDuration": "400",
                                "hideDuration": "1000",
                                "timeOut": "7000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.warning('ALERTE', "Il se pourrait qu'il ait un problème");
                        }
                    });
                    //alert($form.serialize());
                }
            },
            cancel: function () {
                $.alert('Canceled!');
            }
        }
    });

}

function addDocumentTab1() {

    $("#tbody1").append(
            "<tr>"
            + "<td></td>"
            + "<td>"
            + '<select data-placeholder="Selectionnez le rôle de cet client..." class="chosen-select">'
            + '<option value="">CNI</option>'
            + '</select>'
            + '</td>'
            + '<td>'
            + '<input type="text" placeholder="entrez le numero de la pièce" class="form-control">'
            + '</td>'
            + '<td>'
            + '<input type="file" placeholder="télécharger le fichier" class="form-control">'
            + '</td>'
            + '<td>'
            + '<button class="btn btn-circle btn-warning" title="supprimer le fichier"><span class="fa fa-eraser"></span></button>'
            + '<button class="btn btn-circle btn-primary"><span class="fa fa-plus"></span></button>'
            + '</td>'
            + '</tr>'
            )
}

function addDocumentTab2() {

    $("#tbody2").append(
            "<tr>"
            + "<td></td>"
            + "<td>"
            + '<select data-placeholder="Selectionnez le rôle de cet client..." class="chosen-select">'
            + '<option value="">CNI</option>'
            + '</select>'
            + '</td>'
            + '<td>'
            + '<input type="text" placeholder="entrez le numero de la pièce" class="form-control">'
            + '</td>'
            + '<td>'
            + '<input type="file" placeholder="télécharger le fichier" class="form-control">'
            + '</td>'
            + '<td>'
            + '<button class="btn btn-circle btn-warning" title="supprimer le fichier"><span class="fa fa-eraser"></span></button>'
            + '<button class="btn btn-circle btn-primary"><span class="fa fa-plus"></span></button>'
            + '</td>'
            + '</tr>'
            )
}

function deleteClient(idClient) {

    $.confirm({
        title: 'Confirm!',
        content: 'Simple confirm!',
        draggable: true,
        buttons: {

            confirm: {
                btnClass: 'btn-green',
                action: function () {
                    $.ajax({
                        url: "http://localhost/apimanagecustomer/public/delClient", //Page de traitement
                        type: 'POST',
                        data: {id: idClient},
                        dataType: 'json',
                        //contentType: false,
                        //processData: false,
                        //cache: false,
                        success: function (data, textStatus, jqXHR) {
                            console.log(data);

                            console.log(jqXHR);
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "progressBar": true,
                                "preventDuplicates": false,
                                "positionClass": "toast-top-right",
                                "onclick": null,
                                "showDuration": "400",
                                "hideDuration": "200",
                                "timeOut": "700",
                                "extendedTimeOut": "100",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut",
                                "onHidden": function () {
                                    $url = "http://localhost/managecustomer/index.php/welcome/listeclient";
                                    window.location.href = $url;
                                }
                            }
                            toastr.success('nouvel agent crée avec succès', 'SUCCES');



                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(errorThrown);

                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "progressBar": true,
                                "preventDuplicates": false,
                                "positionClass": "toast-top-right",
                                "onclick": null,
                                "showDuration": "400",
                                "hideDuration": "1000",
                                "timeOut": "7000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.warning('ALERTE', "Il se pourrait qu'il ait un problème");
                        }
                    });
                }
            },
            cancel: function () {
                $.alert('Canceled!');
            }/*,
             somethingElse: {
             text: 'Something else',
             btnClass: 'btn-blue',
             keys: ['enter', 'shift'],
             action: function () {
             $.alert('Something else?');
             }
             }*/
        }
    });
    /**/
}

function RecupererTouslesFile(numerodossier) {

    $('#tbody1 tr').each(function () {

        var File = $(this).find("td").eq(3).find('input').prop('files')[0];
        var FileName = $(this).find("td").eq(3).find('input').prop('files')[0].name.replace(/ /g, "-");
        console.log(File);
        alert(FileName);

        var formData = new FormData();
        formData.append("file", File);
        formData.append("dossier", numerodossier);

        /*$.ajax({
         
         url: 'http://server-pc/images/index.php',
         type: "POST",
         data : formData,
         
         success:function(data){
         
         console.log(data);
         },
         error:function(data){
         
         },
         
         mimeType: "multipart/form-data",
         cache: false,
         contentType:false,
         processData:false
         
         }); */

    });
}


