/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    setConnexion();

});

function setConnexion() {

    $('#form-login').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            loginUSer: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Vous devez saisir votre login'
                    },
                    stringLength: {
                        min: 3,
                        max: 30,
                        message: 'la longueur du nom doit être spérieure à 3 caractères'
                    }
                }
            },
            motdepasse: {
                validators: {
                    notEmpty: {
                        message: 'Vous devez saisir votre mot de passe'
                    },
                    stringLength: {
                        min: 3,
                        max: 100,
                        message: 'la longueur du nom d\'utilisateur doit être spérieure à 3 caractères'
                    }
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // $(e.target) --> The form instance
        // $(e.target).data('bootstrapValidator')
        //             --> The BootstrapValidator instance
        //             
        //Empecher la soumission par defaut du formulaire
        e.preventDefault();

        $form = $(e.target);
        $form.data('bootstrapValidator').resetForm();

        var bf = $form.data('bootstrapValidator');
        //console.log($form.serialize());
        //console.log($form.attr('action'));

        // Do something ...
        login();


    });
}

function login() {

    $.ajax({
        url: $form.attr('action'), //Page de traitement
        type: 'POST',
        data: $form.serialize(),
        dataType: 'json',
        //contentType: false,
        //processData: false,
        //cache: false,
        success: function (data, textStatus, jqXHR) {
            console.log(data);
            
            if (data["message"] === "aucune correspondance") {

                console.log(jqXHR);
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "progressBar": true,
                    "preventDuplicates": false,
                    "positionClass": "toast-top-right",
                    "onclick": null,
                    "showDuration": "400",
                    "hideDuration": "500",
                    "timeOut": "7000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut",
                    /*"onHidden": function () {
                     $url = "http://localhost/managecustomer/index.php/welcome/openSession";
                     window.location.href = $url;
                     }*/
                }
                toastr.warning('Veuillez verifier vos identifiants', 'ALERTE');

            } else {
                var id = data['data']['idpersonne'];
                console.log(jqXHR);
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "progressBar": true,
                    "preventDuplicates": false,
                    "positionClass": "toast-top-right",
                    "onclick": null,
                    "showDuration": "400",
                    "hideDuration": "1000",
                    "timeOut": "7000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut",
                    "onHidden": function () {
                        $url = "http://localhost/managecustomer/index.php/welcome/openSession?id="+id;
                        window.location.href = $url;
                    }
                }
                toastr.success('Accès correctes, connexion réussie', 'SUCCES');
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr.warning('ALERTE', "Il se pourrait qu'il ait un problème");
        }
    });

}