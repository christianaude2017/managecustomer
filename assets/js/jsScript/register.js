/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    setRegister();

});

function setRegister() {

    $('#form-register').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            denomination: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Vous devez saisir le nom de votre structure'
                    },
                    stringLength: {
                        min: 3,
                        max: 30,
                        message: 'la longueur du nom doit être spérieure à 3 caractères'
                    }
                }
            },
            nomUser: {
                validators: {
                    notEmpty: {
                        message: 'Vous devez saisir un nom d\'utilisateur'
                    },
                    stringLength: {
                        min: 3,
                        max: 100,
                        message: 'la longueur du nom d\'utilisateur doit être spérieure à 3 caractères'
                    }
                }
            },
            prenomUser: {
                validators: {
                    notEmpty: {
                        message: 'Vous devez saisir un prenom d\'utilisateur'
                    },
                    stringLength: {
                        min: 3,
                        max: 100,
                        message: 'la longueur du prenom d\'utilisateur doit être spérieure à 3 caractères'
                    }
                }
            },
            loginUser: {
                validators: {
                    notEmpty: {
                        message: 'Vous devez saisir un login d\'utilisateur'
                    },
                    stringLength: {
                        min: 3,
                        max: 100,
                        message: 'la longueur du login d\'utilisateur doit être spérieure à 3 caractères'
                    }
                }
            },
            motdepasse: {
                validators: {
                    notEmpty: {
                        message: 'Vous devez renseignez un mot de passe svp'
                    },
                    stringLength: {
                        min: 3,
                        max: 100,
                        message: 'la longueur du nom d\'utilisateur doit être spérieure à 3 caractères'
                    },
                    identical: {
                        field: 'motdepasse_confirm',
                        message: 'Les deux mots de passe ne correspondent pas'
                    }
                }
            },
            cmotdepasse: {
                validators: {
                    notEmpty: {
                        message: 'Vous devez renseignez un mot de passe svp'
                    },
                    stringLength: {
                        min: 3,
                        max: 100,
                        message: 'la longueur du nom d\'utilisateur doit être spérieure à 3 caractères'
                    },
                    identical: {
                        field: 'motdepasse',
                        message: 'Les deux mots de passe ne correspondent pas'
                    }
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // $(e.target) --> The form instance
        // $(e.target).data('bootstrapValidator')
        //             --> The BootstrapValidator instance
        //             
        //Empecher la soumission par defaut du formulaire
        e.preventDefault();

        $form = $(e.target);
        $form.data('bootstrapValidator').resetForm();

        var bf = $form.data('bootstrapValidator');
        //alert($form.serialize());
        //console.log($form.attr('action'));

        // Do something ...
        incription();


    });
}

function incription() {

    $.confirm({
        title: 'Confirm!',
        content: 'soumettre votre inscription',
        buttons: {
            confirm: {
                btnClass: 'btn-danger',
                action: function () {
                    $.ajax({
                        url: $form.attr('action'), //Page de traitement
                        type: 'POST',
                        data: $form.serialize(),
                        dataType: 'json',
                        //contentType: false,
                        //processData: false,
                        //cache: false,
                        success: function (data, textStatus, jqXHR) {
                            console.log(data);

                            console.log(jqXHR);
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "progressBar": true,
                                "preventDuplicates": false,
                                "positionClass": "toast-top-right",
                                "onclick": null,
                                "showDuration": "400",
                                "hideDuration": "1000",
                                "timeOut": "7000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut",
                                "onHidden": function () {
                                    $url = "http://localhost/managecustomer/index.php/welcome/connexion";
                                    window.location.href = $url;
                                }
                            }
                            toastr.success('nouvel agent crée avec succès', 'SUCCES');



                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(errorThrown);

                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "progressBar": true,
                                "preventDuplicates": false,
                                "positionClass": "toast-top-right",
                                "onclick": null,
                                "showDuration": "400",
                                "hideDuration": "1000",
                                "timeOut": "7000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.warning('ALERTE', "Il se pourrait qu'il ait un problème");
                        }
                    });
                }
            },
            cancel: function () {
                $.alert('Canceled!');
            }
        }
    });
}