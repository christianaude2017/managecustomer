/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    setProspection();

});

function setProspection() {

    $('#form-prospect').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nomProspect: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Vous devez saisir un nom'
                    },
                    stringLength: {
                        min: 3,
                        max: 30,
                        message: 'la longueur du nom doit être spérieure à 3 caractères'
                    }
                }
            },
            prenomProspect: {
                validators: {
                    notEmpty: {
                        message: 'Vous devez saisir un prenom'
                    },
                    stringLength: {
                        min: 3,
                        max: 100,
                        message: 'la longueur du prenom doit être spérieure à 3 caractères'
                    }
                }
            },
            /*societeProspect: {
             validators: {
             notEmpty: {
             message: 'Vous devez saisir un prenom'
             },
             stringLength: {
             min: 3,
             max: 100,
             message: 'la longueur du prenom doit être spérieure à 3 caractères'
             }
             }
             },*/
            /*fonctionProspect: {
             validators: {
             notEmpty: {
             message: 'Vous devez saisir une fonction'
             },
             stringLength: {
             min: 3,
             max: 100,
             message: 'la longueur du prenom doit être spérieure à 3 caractères'
             }
             }
             },*/
            telProspect: {
                validators: {
                    notEmpty: {
                        message: 'Vous devez renseignez le contact svp'
                    },
                    digits: {
                        message: 'le montant doit contenir seulement que des chiffres'
                    }
                }
            },
            emailProspect: {
                validators: {
                    /*notEmpty: {
                     message: 'Vous devez choisir un sexe'
                     },*/
                    emailAddress: {
                        message: 'Vous devez entrez une email valide'
                    }
                }
            },

            dateProspect: {
                validators: {
                    notEmpty: {
                        message: 'Vous devez choisir la date de prise de la rencontre'
                    }
                }
            },
            dateRDV: {
                validators: {
                    notEmpty: {
                        message: 'Vous devez choisir la date du prochain rendez vous'
                    }
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // $(e.target) --> The form instance
        // $(e.target).data('bootstrapValidator')
        //             --> The BootstrapValidator instance
        //             
        //Empecher la soumission par defaut du formulaire
        e.preventDefault();

        $form = $(e.target);
        $form.data('bootstrapValidator').resetForm();

        var bf = $form.data('bootstrapValidator');
        //alert($form.serialize());
        //console.log($form.attr('action'));

        // Do something ...
        /*if ($('#idpersonne').val() == "") {
         insertAgent();
         } else {
         modifierAgent();
         }*/
        if ($('#idprospection').val() == "") {
            insertProspect();
        } else {
            modifierProspect();
        }



    });
}

function getAgentById(idpersonne) {

    $.ajax({
        url: "http://localhost/apimanagecustomer/public/getagentbyid", //Page de traitement
        type: 'POST',
        data: {
            id: idpersonne
        },
        dataType: 'json',
        //contentType: false,
        //processData: false,
        //cache: false,
        success: function (data, textStatus, jqXHR) {

            console.log(data);
            var data = JSON.stringify(data);

            //console.log(jqXHR);
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-bottom-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "onHidden": function () {
                    $url = "http://localhost/managecustomer/index.php/welcome/modifieragent/?d=" + data;
                    window.location.href = $url;
                }
            }
            toastr.success('INFO', 'Nouvel agent ajoutée');



        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr.warning('ALERTE', "Il se pourrait qu'il ait un problème");
        }
    });
}

function modifierProspect() {

    // Do something ...
    //alert("Bonsoir");
    $.ajax({
        url: $form.attr('action'), //Page de traitement
        type: 'POST',
        data: $form.serialize(),
        dataType: 'json',
        //contentType: false,
        //processData: false,
        //cache: false,
        success: function (data, textStatus, jqXHR) {
            console.log(data);

            console.log(jqXHR);
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "onHidden": function () {
                    $.confirm({
                        title: 'Confirm!',
                        content: 'Voulez-vous enregistrer un autre prospect ?',
                        draggable: true,
                        buttons: {
                            confirm: {
                                btnClass: 'btn-green',
                                action: function () {
                                    $url = "http://localhost/managecustomer/index.php/welcome/ajoutprospection";
                                    window.location.href = $url;
                                }
                            }, cancel: function () {
                                $url = "http://localhost/managecustomer/index.php/welcome/listeprospection";
                                window.location.href = $url;
                            }
                        },
                    });
                }
            }
            toastr.success('Modification effectuée', 'SUCCES');



        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr.warning('ALERTE', "Il se pourrait qu'il ait un problème");
        }
    });

}

function insertProspect() {

    //alert("Bonjour");
    $.confirm({
        title: 'Confirm!',
        content: 'Ajout Nouveau prospect',
        buttons: {
            confirm: {
                btnClass: 'btn-danger',
                action: function () {
                    $.ajax({
                        url: $form.attr('action'), //Page de traitement
                        type: 'POST',
                        data: $form.serialize(),
                        dataType: 'json',
                        //contentType: false,
                        //processData: false,
                        //cache: false,
                        success: function (data, textStatus, jqXHR) {
                            console.log(data);

                            console.log(jqXHR);
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "progressBar": true,
                                "preventDuplicates": false,
                                "positionClass": "toast-top-right",
                                "onclick": null,
                                "showDuration": "400",
                                "hideDuration": "1000",
                                "timeOut": "7000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut",
                                "onHidden": function () {
                                    $.confirm({
                                        title: 'Confirm!',
                                        content: 'Voulez-vous enregistrer un contrat ?',
                                        draggable: true,
                                        buttons: {
                                            confirm: {
                                                btnClass: 'btn-green',
                                                action: function () {
                                                    $url = "http://localhost/managecustomer/index.php/welcome/ajoutcontrat";
                                                    window.location.href = $url;
                                                }
                                            }, cancel: function () {
                                                $url = "http://localhost/managecustomer/index.php/welcome/listeprospection";
                                                window.location.href = $url;
                                            }
                                        }
                                    });
                                }

                            }
                            toastr.success('nouvel insertion crée avec succès', 'SUCCES');
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(errorThrown);

                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "progressBar": true,
                                "preventDuplicates": false,
                                "positionClass": "toast-top-right",
                                "onclick": null,
                                "showDuration": "400",
                                "hideDuration": "1000",
                                "timeOut": "7000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.warning('ALERTE', "Il se pourrait qu'il ait un problème");
                        }
                    });
                    //alert($form.serialize());
                }
            },
            cancel: function () {
                $.alert('Canceled!');
            }
        }
    });

}

function deletepros(idprospection) {

    $.confirm({
        title: 'Confirm!',
        content: 'Simple confirm!',
        draggable: true,
        buttons: {

            confirm: {
                btnClass: 'btn-green',
                action: function () {
                    $.ajax({
                        url: "http://localhost/apimanagecustomer/public/delprospectbyid", //Page de traitement
                        type: 'POST',
                        data: {id: idprospection},
                        dataType: 'json',
                        //contentType: false,
                        //processData: false,
                        //cache: false,
                        success: function (data, textStatus, jqXHR) {
                            console.log(data);

                            console.log(jqXHR);
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "progressBar": true,
                                "preventDuplicates": false,
                                "positionClass": "toast-top-right",
                                "onclick": null,
                                "showDuration": "400",
                                "hideDuration": "200",
                                "timeOut": "700",
                                "extendedTimeOut": "100",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut",
                                "onHidden": function () {
                                    $url = "http://localhost/managecustomer/index.php/welcome/listeprospection";
                                    window.location.href = $url;
                                }
                            }
                            toastr.success('nouvel agent crée avec succès', 'SUCCES');



                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(errorThrown);

                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "progressBar": true,
                                "preventDuplicates": false,
                                "positionClass": "toast-top-right",
                                "onclick": null,
                                "showDuration": "400",
                                "hideDuration": "1000",
                                "timeOut": "7000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.warning('ALERTE', "Il se pourrait qu'il ait un problème");
                        }
                    });
                }
            },
            cancel: function () {
                $.alert('Canceled!');
            }/*,
             somethingElse: {
             text: 'Something else',
             btnClass: 'btn-blue',
             keys: ['enter', 'shift'],
             action: function () {
             $.alert('Something else?');
             }
             }*/
        }
    });
    /**/
}